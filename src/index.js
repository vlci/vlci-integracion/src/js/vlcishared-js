export {
  configureDataGaugeCommon,
  configureDataGaugeMonthCommon,
  configureDataLine,
  configureDataBar,
} from './data/common-process-data';
export {
  resultsetWithFields,
  printDataInHTMLElements,
  getMaxFieldNumberFromData,
  updateDomElement,
} from './data/common-data';
export { splitDataset } from './residuos/common-data';
export {
  getMonth,
  formatMonthAnyo,
  formatDayMonthYear,
  formatShortMonthYear,
  formatTrimestre,
  formatShortMonthShortYear,
  getMonthList,
} from './dates/common-date';
export {
  selectFilterAll,
  resetFilter,
  selectConfigDatasource,
  getSelectCommonOptions,
  getSelectTwoColumnsOptions,
  resetFilterDates,
  resetFilterDate,
  convertToLanguageSelectorDateRange,
  initValueDefaultDate,
  selectValueDefaultInFilter,
  selectConfigDatasourceAllValue,
  resetFilterToDefault,
} from './selector/common-selector';
export {
  initTable,
  formatNameHeaderColumn,
  formatCellMultiplesLines,
  traverseRows,
  convertToTableTreeView,
  downloadCSVTable,
  downloadPDFTable,
  checkAllGroupsCollapsed,
} from './tables/common-table';
export { buildTableCalidadAire } from './medioambiente/table-calidad-aire';
export { initialCalls } from './initial-calls/common-initial-calls';
export {
  calculatePentahoPath,
  getEnvironment,
} from './navigation/common-navigation';
export { formatName } from './formatting/common-text';
export {
  addPointInteger,
  formatToMillions,
  formatToTons,
} from './formatting/common-number';
export {
  printNoDataChart,
  printDataChart,
  formatTooltip,
  initChart,
  initElement,
  calculateXAxisStart,
  calculateMaxYAxis,
  formatTooltipPie,
  formatRoundTooltip,
  formatMilMillionEuros,
} from './charts/common-chart';
export { buildCamarasTrafico } from './camaras/camaras-trafico';
export {
  initStackedBarChart,
  initStackedBarChartMonthSeries,
  updateStackedBarOptions,
} from './charts/stacked-bar/stacked-bar';
export { initBarChart } from './charts/bar-chart';
export { gaugeChart } from './energiaCommonCharts/gauge-chart';
export { chartTop10 } from './energiaCommonCharts/chart-top10';
export { chartBar } from './energiaCommonCharts/bar-chart-energia';
export { distributionChart } from './energiaCommonCharts/distribution-chart';
export { initBarChartComparationMonth } from './charts/bar-chart-comparation-month';
export { loadIframeOnClickEvent } from './iframes/load-iframes';
export { updateObject } from './data/common-data';
export {
  configureLinesChartValidates,
  configureDataValidate,
} from './charts/lines-chart-datas-validate';
export { THRESHOLDS } from './constants/constants';
