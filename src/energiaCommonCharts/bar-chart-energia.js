/**
* Configura y renderiza un gráfico de barras utilizando ECharts.
*
 * @param {string} text - El título principal del gráfico.
 * @param {string} subtext - El subtítulo del gráfico.
 * @param {Array<string>} periodos - Los valores del eje X, representando los periodos o categorías.
 * @param {Array<number>} energiaTotal - Los valores de energía total (kWh) asociados a cada periodo, que se mostrarán como barras.
 * * @param {Array<string>} idChart - id del componente a renderizar del HTML
 *
 * Detalles:
 * - El gráfico se inicializa en un elemento HTML con el id 'chartDesagregatAnual'.
 * - La configuración incluye personalización de estilos para el título, subtítulo, leyenda, y herramientas.
 * - Se utiliza una cuadrícula sin márgenes, optimizada para contener etiquetas y datos.
 * - La herramienta 'Guardar como imagen' permite descargar el gráfico.
 * - La barra muestra valores energéticos con formato 'kWh' en el eje Y.
 *
 * Uso:
 * Llama a esta función proporcionando los datos necesarios para visualizar un gráfico de barras dinámico y estilizado.
 */
export function chartBar(text, subtext, periodos, energiaTotal, idChart) {
    const myChart = echarts.init(document.getElementById(idChart));
    const option = {
      grid: {
        left: '0',
        right: '0',
        bottom: '0',
        top: '60',
        containLabel: true,
      },
      title: {
        text: text,
        margin: '0',
        padding: 0,
        top: '0',
        left: '50%',
        textAlign: 'center',
        itemGap: 5,
        textStyle: {
          fontSize: '15',
          lineHeight: 15,
          fontFamily: 'Montserrat',
          fontWeight: '500',
          color: '#666',
        },
        subtext: subtext,
        subtextStyle: {
          fontFamily: 'Montserrat',
          fontSize: 12,
          lineHeight: 12,
          fontWeight: 500,
          color: '#666',
          width: '100%',
        },
      },
      toolbox: {
        show: true,
        right: '0',
        top: '0',
        feature: {
          saveAsImage: {
            title: 'Descragar',
            textStyle: {
              fontSize: 6,
              fontFamily: 'Montserrat',
              fontWeight: 300,
            },
          },
        },
        emphasis: {
          iconStyle: {
            borderColor: '#ffcd00',
            textFill: '#666',
          },
        },
      },
      legend: {
        show: false,
        orient: 'vertical',
        top: '55',
        bottom: '5',
        left: '0',
        padding: [0, 0],
        width: '20%',
        itemHeight: 10,
        itemWidth: 11,
        itemGap: 10,
        textStyle: {
          fontSize: '12',
          lineHeight: 14,
          fontFamily: 'Montserrat',
          fontWeight: '400',
          color: '#666',
          width: 210,
          overflow: 'break',
        },
      },
      tooltip: {
        trigger: 'axis',
        confine: true,
        axisPointer: {
          type: 'none',
        },
        textStyle: {
          fontFamily: 'Montserrat',
          fontSize: 12,
          fontWeight: 500,
        },
      },
      xAxis: {
        data: periodos,
        axisLabel: {
          show: true,
          margin: 6,
          textStyle: {
            fontSize: 11,
            fontFamily: 'Montserrat',
            fontWeight: '500',
          },
        },
      },
      yAxis: {
        type: 'value',
        alignTicks: true,
        scale: true,
        axisLabel: {
          formatter: '{value} kWh',
          textStyle: {
            fontSize: 11,
            fontFamily: 'Montserrat',
            fontWeight: 400,
          },
        },
      },
      series: [
        {
          data: energiaTotal,
          type: 'bar',
          itemStyle: {
            color: '#4BA3C3', 
          },
        },
      ],
    };
    myChart.setOption(option);
}   