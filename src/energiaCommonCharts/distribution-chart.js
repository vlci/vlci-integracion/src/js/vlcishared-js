/**
 * Función para inicializar y configurar las opciones del gráfico.
 * Esta función configura el gráfico con parámetros personalizables para la cuadrícula, el título, las herramientas emergentes, los ejes y las series.
 * También maneja el redimensionamiento de la ventana para hacer el gráfico responsivo.
 * 
 * @param {string} text - El texto del título principal para el gráfico.
 * @param {string} firstDate - El subtítulo que se muestra debajo del título principal.
 * @param {string} messageToolTips - El formato del mensaje de las herramientas emergentes (tooltips).
 * @param {string} idChart -  id del componente de html.
 * @param {string} consums -  parametros que van en el eje de las x.
 * @param {string} data -  datos que se van a mostrar en el gráfico.deben ir en orde ejem: [[0,0,100],[0,1,150],[0,2,200]] primer valor eje y, segundo valor orden x, tercer valor tamaño del punto.
 * @param {string} days -  parametro string que se ve en el eje y.
 */
export function distributionChart(
    text,
    firstDate,
    messageToolTips,
    idChart,
    consums,
    data,
    days) {
  
  const myChart = echarts.init(document.getElementById(idChart));
  const option = {
    grid: {
      left: '0',
      right: '0',
      bottom: '0',
      top: '50',
      containLabel: true,
    },
    title: {
      text: text,
      margin: '0',
      padding: 0,
      top: '0',
      left: '50%',
      textAlign: 'center',
      itemGap: 5,
      textStyle: {
        fontSize: '15',
        lineHeight: 15,
        fontFamily: 'Montserrat',
        fontWeight: '500',
        color: '#666',
      },
      subtext: firstDate,
      subtextStyle: {
        fontFamily: 'Montserrat',
        fontSize: 12,
        lineHeight: 12,
        fontWeight: 500,
        color: '#666',
        width: '100%',
      },
    },
    toolbox: {
      show: true,
      right: '0',
      top: '0',
      feature: {
        saveAsImage: {
          title: 'Descargar',
          textStyle: {
            fontSize: 6,
            fontFamily: 'Montserrat',
            fontWeight: 300,
          },
        },
      },
      emphasis: {
        iconStyle: {
          borderColor: '#ffcd00',
          textFill: '#666',
        },
      },
    },
    legend: { show: false },
    tooltip: {
      textStyle: {
        fontFamily: 'Montserrat',
        fontSize: 12,
        color: '#666',
        fontWeight: 500,
      },
      formatter: function (params) {
        return (
          params.value[2] + messageToolTips + consums[params.value[0]] + days
        );
      },
    },
    xAxis: {
      type: 'category',
      data: consums,
      boundaryGap: ['20%', '20%'],
      gridIndex: 0,
      offset: 0,
      axisTick: { show: false },
      axisLine: {
        lineStyle: {
          color: '#ccc',
        },
      },
      axisLabel: {
        show: true,
        interval: 0,
        rotate: 0,
        padding: 0,
        margin: 10,
        fontSize: 10,
        lineHeight: 13,
        fontFamily: 'Montserrat',
        fontWeight: 400,
        color: '#333',
      },
    },
    yAxis: {
      type: 'category',
      data: days,
      axisLine: {
        show: false,
      },
      axisTick: { show: false },
      axisLabel: {
        show: true,
        fontSize: 10,
        fontFamily: 'Montserrat',
        fontWeight: 500,
        color: '#333',
      },
    },
    series: [
      {
        name: 'Consum Energètic',
        type: 'scatter',
        color: '#4BA3C3', // azul palido
        symbolSize: function (val) {
          return val[2] * 11;
        },
        data: data,
        animationDelay: function (idx) {
          return idx * 5;
        },
      },
    ],
  };

  // Apply chart option if the option is an object
  if (option && typeof option === 'object') {
    myChart.setOption(option);
  }

  // Resize chart on window resize
  window.addEventListener('resize', myChart.resize);
}
