//node_modules\@vlci\vlcishared\src\formatting\common-number.js
//C:\Users\jululo\Repositorios\pentaho\Ciudad\js\cdmciutat-js\node_modules\@vlci\vlcishared\src\formatting\common-number.js
/**
 * Función para crear un gráfico de barras distribuidas.
 *
 * @param {string} chartName - El título principal del gráfico.
 * @param {string} firstDate - La fecha que se mostrará como subtítulo del gráfico.
 * @param {Array} dataArray - Un array de datos en el formato [[etiqueta, valor], ...].
 * @param {string} idChart - El ID del contenedor HTML donde se renderizará el gráfico.
 *
 * La función genera un gráfico de barras utilizando las opciones proporcionadas y la
 * biblioteca de gráficos ECharts. Está diseñada para representar datos distribuidos,
 * como consumo energético, donde el eje Y contiene etiquetas (como ubicaciones)
 * y el eje X representa valores (como cantidad de consumo).
 *
 * Detalles del funcionamiento:
 * - Configuración de la cuadrícula (`grid`): Define los márgenes del gráfico.
 * - Título (`title`): Muestra el nombre del gráfico y un subtítulo opcional (fecha).
 * - Tooltip: Despliega información detallada al pasar el cursor sobre una barra.
 * - Eje Y (`yAxis`): Contiene las etiquetas de las categorías, ocultando las etiquetas del eje.
 * - Serie de datos (`series`): Define el tipo de gráfico (barras), el color, y los datos mostrados.
 * - Las etiquetas en las barras están configuradas para truncar texto que exceda el ancho de la barra.
 * - La función utiliza `initBarChart` para renderizar el gráfico en el contenedor especificado.
 */
export function chartTop10 (chartName,firstDate,dataArray,tooltipFormatter) {

  const updateOptions = {
    grid: {
      top: '15%',
      bottom: '3%',
      left: '3%',
      right: '3%',
    },
    title: {
      text: chartName,
      margin: '0',
      padding: 0,
      top: '0',
      left: '50%',
      textAlign: 'center',
      itemGap: 5,
      textStyle: {
        fontFamily: 'Montserrat',
        fontSize: 18,
        fontWeight: 600,
        color: '#666',
      },
      subtext: firstDate,
      subtextStyle: {
        fontFamily: 'Montserrat',
        fontSize: 17,
        fontWeight: 500,
        color: '#666',
      },
    },
    tooltip: {
      textStyle: {
        fontFamily: 'Montserrat',
      },
      renderMode: 'html',
      tooltipFormatter,
    },
    yAxis: {
      data: dataArray.map((element) => element[0]),
      axisLabel: { show: false }, // Oculta el texto en el eje Y si está en las etiquetas
    },
    series: [
      {
        data: dataArray.map((element) => element[1]),
        type: 'bar',
        barWidth: '75%', // Ajusta el ancho relativo de las barras
        barCategoryGap: '20%', // Espaciado entre barras
        label: {
          show: true,
          position: 'insideLeft',
          formatter: '{b}', // Muestra solo el nombre de la calle
          width: 400, // Máximo ancho de las etiquetas
          overflow: 'truncate', // Corta el texto si supera el ancho
          textStyle: {
            fontSize: 13,
            fontWeight: 600,
            fontFamily: 'Montserrat',
            color: '#284B63', // Marino claro
          },
        },
        itemStyle: {
          color: '#82bed4', // Azul claro para todas las barras
        },
      },
    ],
  };
  return updateOptions;
}