/**
 * Función para generar un gráfico de tipo "gauge" (medidor) utilizando la librería ECharts.
 * Este gráfico muestra un valor específico dentro de un rango definido, con un diseño personalizado.
 * 
 * Parámetros:
 * @param {number} dataValue - Valor numérico que se mostrará en el gráfico (por ejemplo, nivel, porcentaje, etc.).
 * @param {string} text - Título principal del gráfico.
 * @param {string} subtext - Subtítulo o descripción adicional que aparecerá debajo del título.
 * @param {object} myChart - Instancia de ECharts donde se renderizará el gráfico.
 * @param {integer} maxValueData - Valor máximo que se puede mostrar en el gráfico.
 * 
 * El gráfico tiene una apariencia personalizada con colores y estilos específicos para:
 * - El título y subtítulo.
 * - El progreso del medidor.
 * - Los ejes y las divisiones.
 * 
 * La función configura los parámetros de la visualización y los pasa a la instancia de ECharts (`myChart`) para su renderización.
 * El medidor tiene un rango de valores de 0 a 300, con un progreso visualizado en un círculo que se llena según el valor de `dataValue`.
 */
export function gaugeChart(dataValue, dataUnit, text, subtext,idChart, maxValueData) {
  const myChart = echarts.init(document.getElementById(idChart));

   const option = {
    grid: {
      left: '0',
      right: '0',
      bottom: '0',
      top: '50',
      containLabel: true,
    },
    title: {
      text: text,
      margin: '0',
      padding: 0,
      top: '0',
      left: '60%',
      textAlign: 'center',
      itemGap: 5,
      textStyle: {
        fontSize: 15,
        lineHeight: 15,
        fontFamily: 'Montserrat',
        fontWeight: '500',
        color: '#666',
      },
      subtext: subtext,
      subtextStyle: {
        fontFamily: 'Montserrat',
        fontSize: 12,
        lineHeight: 12,
        fontWeight: 500,
        color: '#666',
        width: '100%',
      },
    },
    legend: {
      show: false,
      top: 'bottom',
      textStyle: {
        fontSize: 11,
        fontFamily: 'Montserrat',
        fontWeight: '400',
        color: '#666',
      },
    },
    toolbox: {
      show: true,
      right: '0',
      top: '0',
      feature: {
        saveAsImage: {
          title: 'Descargar',
          textStyle: {
            fontSize: 6,
            fontFamily: 'Montserrat',
            fontWeight: 300,
          },
        },
      },
      emphasis: {
        iconStyle: {
          borderColor: '#ffcd00',
          textFill: '#666',
        },
      },
    },
    series: [
      {
        type: 'gauge',
        center: ['50%', '65%'],
        startAngle: 200,
        endAngle: -20,
        min: 0,
        max: maxValueData,
        splitNumber: 10,
        progress: {
          show: true,
          width: 40,
          itemStyle: {
            color: '#4BA3C3', // azul palido
          },
        },
        pointer: {
          show: false,
        },
        axisLine: {
          lineStyle: {
            width: 40,
          },
        },
        axisTick: {
          distance: -50,
          splitNumber: 5,
          lineStyle: {
            width: 2,
            color: '#ccc',
          },
        },
        splitLine: {
          distance: -55,
          length: 9,
          lineStyle: {
            width: 2,
            color: '#ccc',
          },
        },
        axisLabel: {
          distance: 0,
          textStyle: {
            color: '#999',
            fontStyle: 'normal',
            fontWeight: '400',
            fontFamily: 'Montserrat',
            fontSize: 12,
          },
        },
        anchor: {
          show: false,
          showAbove: true,
          size: 35,
          itemStyle: {
            borderWidth: 40,
            color: '#284B63', // marino palido
          },
        },
        detail: {
          formatter: dataValue + dataUnit,
          width: '60%',
          borderRadius: 8,
          offsetCenter: [0, 0],
          valueAnimation: true,
          textStyle: {
            color: '#284B63', // marino palido
            fontStyle: 'normal',
            fontWeight: '600',
            fontFamily: 'Montserrat',
            fontSize: 30,
          },
        },
        data: [
          {
            value: dataValue,
          },
        ],
      },
    ],
  };

  // Set the options to the chart
  myChart.setOption(option);
}