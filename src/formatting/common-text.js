/**
 * Separa las cadenas para su correcta representición.
 * @param {String} name Nombre a formatear.
 * @param {Integer} size numero de caracteres que como máximo se mostrarán en cada fila.
 * @returns Cadena formateada.
 */
export function formatName(name, size = 29) {
  if (name.length >= size) {
    const words = name.split(' ');
    let currentLine = '';
    let result = '';
    words.forEach((word) => {
      if (`${currentLine} ${word}`.length <= size) {
        currentLine += (currentLine ? ' ' : '') + word;
      } else {
        result += `${currentLine}\n`;
        currentLine = word;
      }
    });
    result += currentLine;
    return result;
  }
  return name;
}
