/**
 * Introduce . cada 3 cifras del número pasado por parámetros: 1234->1.234
 * @param {integer} value Entero al que se le va a poner.
 * @returns String con el número transformado.
 */
export function addPointInteger(value, decimals = 2) {
  // Obtiene el idioma del navegador
  let usrlang = navigator.language || navigator.userLanguage;
  usrlang =
    usrlang.length <= 2 ? `${usrlang}-${usrlang.toUpperCase()}` : usrlang;
  const factorDigits = {
    minimumFractionDigits: 0,
    maximumFractionDigits: decimals,
  };
  return usrlang === 'es-ES'
    ? value.toLocaleString('de-DE', factorDigits)
    : value.toLocaleString(usrlang, factorDigits);
}

/**
 * Función que convierte un número a notación abreviada
 * @param {integer} value Entero que se va a formatear
 * @returns el valor abreviado a millones
 */
export function formatToMillions(value) {
  const million = 1000000;
  return value >= million ? `${value / million}M` : value.toString();
}



/**
 * Función que convierte un número a notación abreviada
 * @param {integer} value Entero que se va a formatear
 * @returns el valor abreviado a toneladas
 */
export function formatToTons(value) {
  const ton = 1000;
  return value >= ton ? `${value / ton} Tn` : value.toString();
}