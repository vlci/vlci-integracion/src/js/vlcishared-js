import { splitData, updateObject } from '../../data/common-data';
import { addPointInteger } from '../../formatting/common-number';
import { initChart } from '../common-chart';
import { getMonth } from '../../dates/common-date';
import { stackedBarCommonOptions } from './stacked-bar-common-options';

/**
 * Método que formatea el tooltip.
 * @param {*} params contine los valores del tooltip.
 * @param {*} seriesData * Contine las serias de valores del tooltip de todos los años.
 * @param {string} unidadMedida unidad de medida del valor
 * @returns el texto del tooltip formateado
 */
const sameMonthAllYearsFormatter = (params, seriesData) => {
  seriesData.sort((a, b) => parseInt(a.anyo) - parseInt(b.anyo));

  const sumsByYear = {};

  // Aggregacíon del los años del mes elegido.
  seriesData.forEach((entry) => {
    const year = entry[1];
    const month = entry[0];

    if (month === params.seriesName && !sumsByYear[year]) {
      sumsByYear[year] = 0;
    }

    if (month === params.seriesName) {
      sumsByYear[year] += entry[2];
    }
  });

  let nameLabel = `<p>${params.seriesName}</p>`;

  for (const [year, sum] of Object.entries(sumsByYear)) {
    nameLabel += `<div>${
      params.marker
    } ${year}: <span style="float: right; margin-left:20px;font-weight: 600;">${addPointInteger(
      sum,
    )}</span></div>`;
  }

  return nameLabel;
};
/**
 * Configura el grafico de barras apiladas a partir de un conjunto inicial de datos.
 * Establece unas opciones predefinidas que pueden ser modificadas proporcionando un objeto a traves de los parametros.
 * @param {text} idChart Nombre del chart en el que se creará el gráfico.
 * @param {array} misDatosArray Conjunto inicial de datos
 * @param {Object} updateOptions Objeto que contiene las opciones que se quieren cambiar de las establecidas por defecto.
 * @param {Object} updateDataOptions Objeto que contiene las opciones que se quieren cambiar de cada dato de la serie individualmente.
 */
export function initStackedBarChart(
  idChart,
  data,
  updateOptions = undefined,
  updateDataOptionsParam = undefined,
) {
  const updateDataOptions =
    updateDataOptionsParam === null ? undefined : updateDataOptionsParam;
  const anyos = new Set(data.map((subArray) => subArray[1]));
  let borderColors;
  /**
   * Observa si el objeto updateDataOptions tiene definido un array dentro de itemStyle.borderColor.
   * Si es asi lo guardará para en el futuro aplicar cada color del array a cada una de las series.
   */
  const prepareBorderColors = () => {
    if (
      updateDataOptions &&
      updateDataOptions.itemStyle &&
      Array.isArray(updateDataOptions.itemStyle.borderColor)
    ) {
      borderColors = [...updateDataOptions.itemStyle.borderColor];
      delete updateDataOptions.itemStyle.borderColor;
    }
  };

  /**
   * Calcula las series de la StackedBarChart, aplicando el estilo updateDataOptions a cada una de las series.
   * @returns
   */
  const calculateSeries = () => {
    const defaultDataOptions = {
      name: 'SIN NOMBRE',
      type: 'bar',
      stack: 'total',
      label: {
        show: true,
        fontFamily: 'Montserrat',
        fontSize: 10,
        formatter: (params) => addPointInteger(Math.round(params.value)),
      },
      itemStyle: undefined,
      emphasis: {
        focus: 'series',
      },
      data: undefined,
    };

    prepareBorderColors();

    const separatedData = splitData(data, 0);
    const categories = Object.keys(separatedData);
    anyos.forEach((anyo) => {
      categories.forEach((category) => {
        const isYear = separatedData[category].find(
          (subArray) => subArray[1] === anyo,
        );
        if (isYear === undefined) {
          separatedData[category].push([
            separatedData[category][0][0],
            anyo,
            undefined,
          ]);
          separatedData[category].sort();
        }
      });
    });
    const series = [];
    Object.keys(separatedData).forEach((category) => {
      if (borderColors) {
        defaultDataOptions.itemStyle = { borderColor: borderColors.shift() };
      }
      defaultDataOptions.name = category;

      defaultDataOptions.data = separatedData[category].map(
        (subArray) => subArray[2],
      );
      let finallyDataOptions = {};
      if (updateDataOptions !== undefined) {
        finallyDataOptions = updateObject(
          defaultDataOptions,
          updateDataOptions,
        );
      } else {
        finallyDataOptions = defaultDataOptions;
      }
      series.push({ ...finallyDataOptions });
    });
    return series;
  };
  const defaultOptions = {
    title: {
      text: 'SIN TITULO',
      left: 'center',
      top: 'top',
      textStyle: {
        fontSize: 20,
        fontFamily: 'Montserrat',
        fontWeight: 600,
        color: '#666',
      },
    },
    tooltip: {
      axisPointer: {
        type: 'shadow',
      },
      trigger: 'item',
      formatter: (params) => sameMonthAllYearsFormatter(params, data),
      textStyle: {
        fontFamily: 'Montserrat',
      },
      position(point, params, dom, rect, size) {
        const position = {};

        const tooltipHeight = size.contentSize[1];
        const halfTooltipHeight = tooltipHeight / 2;
        const chartHeight = size.viewSize[1];
        const pointY = point[1];

        if (pointY < halfTooltipHeight) {
          position.top = 10;
        } else if (pointY + halfTooltipHeight > chartHeight) {
          position.bottom = 10;
        } else {
          position.top = pointY - halfTooltipHeight;
        }

        if (point[0] < size.viewSize[0] / 2) {
          position.left = point[0] + 20;
        } else {
          position.right = size.viewSize[0] - point[0] + 20;
        }

        return position;
      },
    },
    legend: {
      padding: 40,
      textStyle: {
        fontFamily: 'Montserrat',
      },
    },
    grid: {
      left: '3%',
      right: '4%',
      bottom: '3%',
      containLabel: true,
    },
    xAxis: {
      type: 'value',
      splitNumber: 10,
      name: 'SIN NOMBRE',
      nameLocation: 'start',
      nameTextStyle: {
        fontWeight: 'bold',
        fontFamily: 'Montserrat',
        color: '#666',
        padding: -40,
      },
      axisLabel: {
        margin: 5,
        textStyle: {
          fontFamily: 'Montserrat',
        },
        formatter: (value) => addPointInteger(value),
      },
      splitLine: {
        lineStyle: {
          color: '#DDD',
        },
      },
    },
    yAxis: {
      type: 'category',
      data: Array.from(anyos),
      axisLabel: {
        textStyle: {
          fontFamily: 'Montserrat',
        },
      },
    },
    series: calculateSeries(),
    toolbox: {
      show: true,
      feature: {
        restore: { title: 'Restablir' },
        saveAsImage: { title: 'Desar' },
      },
      right: 50,
    },
  };

  let finallyOptions = {};
  if (updateOptions !== undefined) {
    finallyOptions = updateObject(defaultOptions, updateOptions);
  } else {
    finallyOptions = defaultOptions;
  }
  initChart(idChart, finallyOptions);
}

export function updateStackedBarOptions(specificOptions) {
  let modifiedOptions = {};
  modifiedOptions = updateObject(stackedBarCommonOptions, specificOptions);
  return modifiedOptions;
}

/**
 * Configura el grafico de barras apiladas a partir de un conjunto inicial de datos. Esta función se utiliza cuando las series son meses.
 * Establece unas opciones predefinidas que pueden ser modificadas proporcionando un objeto a traves de los parametros.
 * En concreto este tipo de graficos utiliza unos colores en común que son definidos aqui mismo.
 * @param {text} idChart Nombre del chart en el que se creará el gráfico.
 * @param {array} misDatosArray Conjunto inicial de datos
 * @param {Object} updateOptions Objeto que contiene las opciones que se quieren cambiar de las establecidas por defecto.
 * @param {Object} updateDataOptions Objeto que contiene las opciones que se quieren cambiar de cada dato de la serie individualmente.
 */
export function initStackedBarChartMonthSeries(
  idChart,
  data,
  updateOptionsParam = {},
  updateDataOptionsParam = {},
) {
  /**
   * Ordena los años del eje y para mostrarlos adecuadamente
   * @returns Un array con los años ordenado de mayor a menor.
   */
  function getYearsOrdered() {
    const yearsSet = new Set();
    data.forEach((element) => {
      yearsSet.add(element[1]);
    });
    const orderedYears = Array.from(yearsSet).sort((a, b) => a - b);

    return orderedYears;
  }

  // Opciones especificas
  const updateOptions = {
    color: [
      '#81B1CC',
      '#ABD9EA',
      '#C2EEF9',
      '#F9E2AF',
      '#F9C780',
      '#FDAE61',
      '#F46D43',
      '#FDAE61',
      '#F9E2AF',
      '#C2EEF9',
      '#ABD9EA',
      '#81B1CC',
    ],
    yAxis: {
      data: getYearsOrdered(),
    },
  };

  let finallyOptions = {};
  finallyOptions = updateObject(updateOptionsParam, updateOptions);

  // Opciones especificas sobre los datos
  const updateDataOptions = {
    itemStyle: {
      borderWidth: 2,
      borderColor: [
        '#448DAD',
        '#89CADB',
        '#A0DEE8',
        '#F4D28C',
        '#F4B569',
        '#F79646',
        '#F25930',
        '#F79646',
        '#F4D28C',
        '#A0DEE8',
        '#89CADB',
        '#448DAD',
      ],
    },
  };

  const finallyDataOptions =
    updateDataOptionsParam === null
      ? updateDataOptions
      : updateObject(updateDataOptionsParam, updateDataOptions);

  // Sustución del número de mes por el nombre
  const formatedData = data;
  formatedData.forEach((element, index) => {
    formatedData[index][0] = getMonth(element[0]);
  });

  initStackedBarChart(
    idChart,
    formatedData,
    finallyOptions,
    finallyDataOptions,
  );
}
