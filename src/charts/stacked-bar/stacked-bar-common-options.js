import {
  formatToMillions,
} from '../../formatting/common-number';


/**
 * Opciones por defecto para todos los graficos stacked bar
 */
export const stackedBarCommonOptions = {
  grid: {
    containLabel: true,
  },
  title: {
    left: 'center',
    top: '0',
    textStyle: {
      fontSize: 15,
      fontFamily: 'Montserrat',
      fontWeight: 500,
      color: '#666',
    },
  },
  legend: {
    orient: 'horizontal',
    bottom: '0',
    left: 'center',
    width: '100%',
    textStyle: {
      fontSize: 11,
      fontFamily: 'Montserrat',
      fontWeight: 400,
      color: '#666',
    },
  },
  toolbox: {
    show: true,
    right: '0',
    top: '0',
    feature: {
      restore: {
        textStyle: {
          fontSize: 6,
          fontFamily: 'Montserrat',
          fontWeight: 300,
        },
      },
      saveAsImage: {
        textStyle: {
          fontSize: 6,
          fontFamily: 'Montserrat',
          fontWeight: 300,
        },
      },
    },
    emphasis: {
      iconStyle: {
        borderColor: '#ffcd00',
        textFill: '#666',
      },
    },
  },
  tooltip: {
    textStyle: {
      fontFamily: 'Montserrat',
      fontSize: 12,
    },
  },
  xAxis: [
    {
      type: 'category',
      axisLabel: {
        show: true,
        interval: 0,
        textStyle: {
          fontFamily: 'Montserrat',
          fontSize: 11,
        },
      },
      data: [],
    },
  ],
  yAxis: [
    {
      type: 'value',
      alignTicks: true,
      position: 'right',
      axisLabel: {
        textStyle: {
          fontSize: 10,
          fontFamily: 'Montserrat',
          fontWeight: 400,
        },
        formatter: (value) => `${value}%`,
      },
    },
    {
      type: 'value',
      position: 'left',
      axisLabel: {
        textStyle: {
          fontSize: 10,
          fontFamily: 'Montserrat',
          fontWeight: 400,
        },
        formatter: (value) => `${formatToMillions(value)}€`,
      },
    },
  ],
};
