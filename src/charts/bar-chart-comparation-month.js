import { initBarChart } from './bar-chart';
import { updateObject } from '../data/common-data';
import { getMonthList } from '../dates/common-date';
import { DEFAULT_LANGUAGE, MONTHS_YEAR } from '../language/common-language';

/**
 * Devuelve el último año del conjunto de datos.
 * @param {array} dataArray Conjunto inicial de datos.
 * @returns último año
 */
function getLastYear(dataArray) {
  return Math.max(...dataArray.map((element) => Number(element[0])));
}

/**
 * Devuelve el valor del último año del conjunto de datos.
 * @param {array} dataArray Conjunto inicial de datos.
 * @returns valor del último año
 */
function getLastYearValues(dataArray) {
  const arrayUltimoAnyo = dataArray.filter(
    (arr) => Number(arr[0]) === getLastYear(dataArray),
  );
  const valoresUltimoAnyo = Array.from({ length: 12 }, () => 0);
  arrayUltimoAnyo.forEach(([anio, mes, valor]) => {
    const mesIndex = parseInt(mes, 10) - 1; // Restar 1 porque los meses comienzan desde 1
    valoresUltimoAnyo[mesIndex] = valor;
  });
  return valoresUltimoAnyo;
}

/**
 * Devuelve el valor del penúltimo año del conjunto de datos.
 * @param {array} dataArray Conjunto inicial de datos.
 * @returns valor del penúltimo año
 */
function getPenultimateYearValues(dataArray) {
  const arrayPenultimoAnyo = dataArray.filter(
    (arr) => Number(arr[0]) === getLastYear(dataArray) - 1,
  );
  const valoresPenultimoAnyo = Array.from({ length: 12 }, () => 0);
  arrayPenultimoAnyo.forEach(([anio, mes, valor]) => {
    const mesIndex = parseInt(mes, 10) - 1; // Restar 1 porque los meses comienzan desde 1
    valoresPenultimoAnyo[mesIndex] = valor;
  });
  return valoresPenultimoAnyo;
}

/**
 * Configura el grafico de barras a partir de un conjunto inicial de datos. Creando una gráfica de barras que compara 2 años.
 * Establece unas opciones predefinidas que pueden ser modificadas proporcionando un objeto a traves de los parametros.
 * @param {text} idChart Nombre del chart en el que se creará el gráfico.
 * @param {array} data Conjunto inicial de datos.
 * @param {Object} specificOptions Objeto que contiene las opciones que se quieren cambiar de las establecidas por defecto.
 */

export function initBarChartComparationMonth(
  idChart,
  dataArray,
  specificOptions = undefined,
) {
  const lastYearString = getLastYear(dataArray).toString();
  const penultimateYearString = (getLastYear(dataArray) - 1).toString();

  const defaultOptions = {
    title: {
      left: 'center',
      top: '-12%',
      textStyle: {
        fontSize: 15,
        fontFamily: 'Montserrat',
      },
    },
    legend: {
      top: '10%',
      show: true,
      data: [penultimateYearString, lastYearString],
      textStyle: {
        fontFamily: 'Montserrat',
      },
    },
    grid: {
      bottom: '8%',
      top: '20%',
      right: '2%',
    },
    xAxis: [
      {
        type: 'category',
        data: getMonthList(MONTHS_YEAR, DEFAULT_LANGUAGE),
        interval: 0,
        margin: 10,
        axisLabel: {
          textStyle: {
            fontSize: 10,
            fontFamily: 'Montserrat',
            fontWeight: 400,
          },
        },
      },
    ],
    yAxis: [
      {
        type: 'value',
        axisLabel: {
          formatter: (value) => value.toLocaleString('es-ES'),
          textStyle: {
            fontSize: 10,
            fontFamily: 'Montserrat',
            fontWeight: 400,
          },
        },
      },
    ],
    series: [
      {
        name: penultimateYearString,
        type: 'bar',
        data: getPenultimateYearValues(dataArray),
      },
      {
        name: lastYearString,
        type: 'bar',
        barGap: 0,
        data: getLastYearValues(dataArray),
      },
    ],
  };

  const mergedOptions = updateObject(defaultOptions, specificOptions);
  initBarChart(idChart, mergedOptions);
}
