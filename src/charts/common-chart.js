import { addPointInteger } from '../formatting/common-number';
import { getMonth } from '../dates/common-date';

export const DIVSIZE = 33;

/**
 * Esta función ejecuta el initFunction que recibe de parámetro si el elementId está
 * disponible en el DOM, si no es así espera a que se cargue el contenido dinámico
 * esperando el custom event del eventName, que por defecto es MainContentLoadedCustom.
 * @param {*} elementId el identificador del elemento en el que se renderizará el componente.
 * @param {*} initFunction la función que renedriza el componente.
 * @param {*} eventName el nombre del evento que hay que esperar.
 */
export function initElement(
  elementId,
  initFunction,
  eventName = 'MainContentLoadedCustom',
) {
  let element = document.getElementById(elementId);

  if (!element) {
    return new Promise((resolve) => {
      document.body.addEventListener(eventName, () => {
        element = document.getElementById(elementId);

        if (element) {
          resolve(initFunction(element));
        } else {
          console.error(
            `El elemento con ID ${elementId} aún no está disponible después del evento.`,
          );
          resolve(null);
        }
      });
    });
  }
  return Promise.resolve(initFunction(element));
}

export function initChart(chartId, option) {
  const initializeChart = (chartElem) => {
    const myChart = echarts.init(chartElem);
    myChart.setOption(option, true);

    // Resize chart on window resize
    window.addEventListener('resize', () => {
      if (myChart != null && myChart !== undefined) {
        myChart.resize();
      }
    });

    // Add event listener to 'main-content' for Bootstrap collapse events
    const mainContent = document.getElementById('main-content');
    if (mainContent) {
      mainContent.addEventListener('shown.bs.collapse', () => {
        myChart.resize();
      });
      mainContent.addEventListener('hidden.bs.collapse', () => {
        myChart.resize();
      });
    }

    return myChart;
  };

  return initElement(chartId, initializeChart);
}

export function printNoDataChart(idHide, idShow) {
  const noDataStyle = "url('./build/images/nodata1.png') no-repeat top center";
  const hideElement = idHide ? document.querySelector(idHide) : null;
  const showElement = idShow ? document.querySelector(idShow) : null;

  if (hideElement) {
    hideElement.hidden = true;
  }
  if (showElement) {
    showElement.style.background = noDataStyle;
    showElement.hidden = false;
  }
}

export function printDataChart(idHide, idShow) {
  const hideElement = idHide ? document.querySelector(idHide) : null;
  const showElement = idShow ? document.querySelector(idShow) : null;

  if (hideElement) {
    hideElement.hidden = true;
  }
  if (showElement) {
    showElement.style.background = '#ffffff';
    showElement.hidden = false;
  }
}

/**
 * Método que formatea el tooltip.
 * @param {string} params contine los valores del tooltip.
 * @param {boolean} roundValues indica si queremos redondear el valor en el tooltip. True quitará decimales redondeando. False no.
 * @param {boolean} serie indica si los valores tienen varias series o no.
 * @param {string} unidadMedida unidad de medida del valor
 * @returns el texto del tooltip formateado
 */
export function formatTooltip(
  params,
  roundValues = false,
  serie = true,
  unidadMedida = '',
) {
  const colorSpan = (color) =>
    `<span style="display:inline-block;margin-right:5px;border-radius:10px;width:9px;height:9px;font-weight: 500;background-color:${color}"></span>`;
  let nameLabel = `<p style="font-weight: 500;color: #565656;">${params[0].name}</p>`;
  params.forEach((item) => {
    const roundedValues = roundValues ? Math.round(item.data) : item.data;
    const valuesLabel = serie
      ? `<div style="font-weight: 400;color: #666;">${colorSpan(item.color)} ${
          item.seriesName
        }: 
      <span style="float: right; margin-left:20px;font-weight: 600;color: #565656;">
            ${
              item.data
                ? `${addPointInteger(roundedValues)} ${unidadMedida}`
                : '-'
            }
          </span>
        </div>`
      : `<div>
          <span style="font-weight: 600;color: #565656;">${
            item.data
              ? `${addPointInteger(roundedValues)} ${unidadMedida}`
              : '-'
          }
          </span>
        </div>`;
    nameLabel += valuesLabel;
  });
  return nameLabel;
}

/**
 * Calcular inicio del datazoom
 * Devuelve el valor en el que comenzará el datazoom de la gráfica
 * @param {int} dataLength longitud de los datos del eje X
 * @returns number que indica donde comenzará el datazoom
 */
export function calculateXAxisStart(dataLength) {
  return (100 * (dataLength - 120)) / dataLength;
}

/**
 * Calcula cual es el máximo del eje Y en la gráfica dependiendo del intervalo
 * que se quiera tener entre los marcadores del eje.
 * Ej: el valor máximo de los datos es 2500 y el intervalo es 1000, el resultado será 3000.
 * @param {*} value el valor del dato máximo de la serie
 * @param {*} interval el intervalo de los ticks del eje Y
 */
export function calculateMaxYAxis(values, interval) {
  const maxValue = Math.max(...values);
  const taskForNextInterval = interval - (maxValue % interval);
  return maxValue + taskForNextInterval;
}

/**
 * Modulo que formatea en funcion del tamaño de la pantalla
 * @param {array} misDatosArray Array con los datos a formatear
 * @param {*} value Valor del campo que está tratando
 * @param {Number} index Indica la posicion del dato a representar dentro de misDatosArray
 * @returns cadena formateada segun las dimensiones de la pantalla
 */
export function formatterReloj(misDatosArray, value, index) {
  const valor = addPointInteger(Math.round(value));
  const month = getMonth(misDatosArray[index][0]);
  const year = misDatosArray[index][1];
  const textBelowGraph = `\n${month} ${year} : ${valor}`;
  const textIntoGraph = `${valor}\n${month} ${year}`;
  return window.innerWidth <= 1710 ? textBelowGraph : textIntoGraph;
}

/**
 * Método que formatea el tooltip de los gráficos pie.
 * @param {Object} params contine los valores del tooltip.
 * @param {boolean} percentage true si el tooltip muestra un porcentage
 * @returns el texto del tooltip formateado
 */
export function formatTooltipPie(params, percent = false) {
  const colorSpan = `<span style="display:inline-block;margin-right:5px;border-radius:10px;width:9px;height:9px;background-color:${params.color}"></span>`;
  const valueLabel = percent
    ? `${params.percent}%`
    : addPointInteger(params.value);

  const nameLabel = `<div> ${colorSpan} ${params.name} <span style="float: right; margin-left:20px;font-weight: 600;color: #333;"> ${valueLabel} </span></div>`;

  return nameLabel;
}

export function formatRoundTooltip(params, isPie) {
  let label = `${params.name} : `;
  label += `<span style="font-weight:500">${addPointInteger(
    Math.round(params.value),
  )}</span>`;
  if (isPie) {
    label += `<span> (${params.percent}%)</span>`;
  }
  return params.marker + label;
}

export function formatMilMillionEuros(value) {
  const ONE_MILLION = 1000000;
  const castedValue = Number(value);

  if (castedValue >= 0 && castedValue < ONE_MILLION) {
    return `${addPointInteger(castedValue)} €`;
  }

  if (castedValue >= ONE_MILLION) {
    return `${addPointInteger(castedValue / ONE_MILLION)} M €`;
  }

  return '-';
}
