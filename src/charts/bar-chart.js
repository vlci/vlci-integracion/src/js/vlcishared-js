import { formatRoundTooltip, initChart } from "./common-chart";
import { updateObject } from '../data/common-data';

/**
 * Configura el grafico de barras a partir de un conjunto inicial de datos.
 * Establece unas opciones predefinidas que pueden ser modificadas proporcionando un objeto a traves de los parametros.
 * @param {text} idChart Nombre del chart en el que se creará el gráfico.
 * @param {array} data Conjunto inicial de datos.
 * @param {Object} updateOptions Objeto que contiene las opciones que se quieren cambiar de las establecidas por defecto.
 */

export function initBarChart(idChart, updateOptions = undefined) {
  const defaultOptions = {
    title: {
      text: 'SIN TITULO',
      left: 'center',
      top: 'top',
      padding: 50,
      textStyle: {
        fontSize: 14,
        fontFamily: 'Montserrat',
        fontWeight: 400,
        color: '#333',
      },
    },
    legend: {
      show: false,
    },
    grid: {
      top: '30%',
      bottom: '5%',
      left: '8%',
      right: '8%',
    },
    xAxis: {
      type: 'value',
      show: false,
    },
    yAxis: {
      show: false,
      type: 'category',
    },
    tooltip: {
      show: true,
      textStyle: {
        fontFamily: 'Montserrat',
        fontWeight: '600',
      },
      appendToBody: true,
      formatter: (params) => formatRoundTooltip(params, false)
    },
    series: [
      {
        type: 'bar',
        showBackground: true,
      },
    ],
  };

  let finallyOptions = {};
  if (updateOptions !== undefined) {
    finallyOptions = updateObject(defaultOptions, updateOptions);
  } else {
    finallyOptions = defaultOptions;
  }
  initChart(idChart, finallyOptions);
}