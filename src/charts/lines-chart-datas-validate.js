import { formatTooltip, initChart } from './common-chart';
import { updateObject } from '../data/common-data';

const ID_COLUMN = 'id';
const DATE_COLUMN = 'fecha';
const VALUE_COLUMN = 'value';

/**
 * Separa en dos listas las fechas con información de datos validados y de datos no validados
 * La función servirá para pintar en el eje X con diferente color la información
 * @param {array} datosOrigen Datos iniciales devueltos por la query
 * @param {array} kpiValidList Lista de entityId que indican las filas que contienen datos validados
 * @param {array} kpiInvalidList Lista de entityId que indican las filas que contienen datos NO validados
 * @returns lista de fechas con datos válidos y lista de fecha con datos validados y con datos no validados
 */
export function separarFechasValidadas(
  datosOrigen,
  kpiValidList,
  kpiInvalidList,
) {
  // Inicializamos los arrays para las fechas validadas y no validadas
  const datesValidateAxisX = [];
  const datesInValidateAxisX = [];

  datosOrigen.forEach((item) => {
    if (kpiValidList.includes(`${item[ID_COLUMN]}`)) {
      datesValidateAxisX.push(`${item[DATE_COLUMN]}`);
    } else if (kpiInvalidList.includes(`${item[ID_COLUMN]}`)) {
      datesInValidateAxisX.push(`${item[DATE_COLUMN]}`);
    }
  });
  return [
    datesValidateAxisX.map((fecha) => [fecha, 0]),
    datesInValidateAxisX.map((fecha) => [fecha, 0]),
  ];
}

/**
 * Obtiene las series de la gráfica diferenciando entre los datos validados y los no validados
 * @param {array} datosOrigen Datos iniciales devueltos por la query
 * @param {array} datosClean Datos del kpiValue
 * @param {array} kpiValidList Lista de entityId que indican las filas que contienen datos validados
 * @param {array} kpiInvalidList Lista de entityId que indican las filas que contienen datos NO validados
 * @returns Series que mostrará la gráfica
 */
export function configureDataValidate(
  datosOrigen,
  datosClean,
  kpiValidList,
  kpiInvalidList,
  language='es_ES'
) {
  const [datesValidateAxisX, datesInValidateAxisX] = separarFechasValidadas(
    datosOrigen,
    kpiValidList,
    kpiInvalidList,
  );

  let dataNoValidateMessage = language == 'es_ES'? 'Datos sin validar': 'Dades sense validar';
  if (datesInValidateAxisX.length === 0) {
    dataNoValidateMessage = '';
  }
  // Pinta la linea gruesa del exe X de los datos validados
  const validatedData = {
    name: 'Datos validados',
    type: 'line',
    data: [...datesValidateAxisX, datesInValidateAxisX[0]],
    lineStyle: {
      color: '#00A9A0',
      width: 4,
    },
    showSymbol: false,
    tooltip: {
      show: false,
    },
  };
  let firstDateInValidateExecX = '';
  let lastDateInValidateExecX = '';
  if (datesInValidateAxisX.length === 0) {
    const ultimaFecha = datosOrigen[datosOrigen.length - 1].fecha;
    firstDateInValidateExecX = ultimaFecha;
    lastDateInValidateExecX = ultimaFecha;
  } else {
    firstDateInValidateExecX = datesInValidateAxisX[0][0];
    lastDateInValidateExecX =
      datesInValidateAxisX[datesInValidateAxisX.length - 1][0];
  }
  const maxValue = Math.max(
    ...datosOrigen
      .filter((item) => item[VALUE_COLUMN] !== null)
      .map((item) => parseInt(`${item[VALUE_COLUMN]}`, 10)),
  );

  const invalidateData = {
    name: 'Datos NO validados',
    type: 'line',
    data: datesInValidateAxisX,
    lineStyle: {
      color: '#80D4CF',
      width: 4,
    },
    showSymbol: false,
    tooltip: {
      show: false,
    },
    markArea: {
      silent: true,
      itemStyle: {
        opacity: 0.6,
        color: '#eee',
      },
      tooltip: {
        show: false,
      },
      label: {
        color: '#333',
        fontStyle: 'normal',
        fontWeight: '400',
        fontFamily: 'Montserrat',
        fontSize: 11,
      },
      data: [
        [
          {
            name: dataNoValidateMessage,
            xAxis: firstDateInValidateExecX,
            yAxis: 0,
          },
          {
            name: 'end',
            xAxis: lastDateInValidateExecX,
            yAxis: maxValue,
          },
        ],
      ],
    },
  };

  // Crear el resultado final incluyendo a las líneas de la gráfica la lines gruesa del eje X
  const result = [...datosClean, validatedData, invalidateData];

  return result;
}

/**
 * Configura las gráficas con datos validados y no validados
 * @param {String} idChart Identificador del HTML que contiene la gráfica
 * @param {array} dataArray Datos iniciales devueltos por la query
 * @param {String} specificOptions Opciones especificas de la gráfica
 * @returns Datos configurados
 */
export function configureLinesChartValidates(
  idChart,
  seriesData,
  specificOptions = undefined,
) {
  const defaultOptions = {
    grid: {
      left: '2.5%',
      right: '5%',
      bottom: '70',
      top: '150',
      //heigh: '90%',
      containLabel: true,
    },
    title: {
      left: 'center',
      top: 'top',
      textStyle: {
        fontSize: 20,
        fontFamily: 'Montserrat',
        fontWeight: 600,
        color: '#666',
      },
    },
    legend: {
      type: 'plain',
      top: '50',
      width: '98%',
      textStyle: {
        fontFamily: 'Montserrat',
      },
    },
    tooltip: {
      trigger: 'axis',
      show: true,
      formatter: (params) => formatTooltip(params),
      textStyle: {
        fontFamily: 'Montserrat',
      },
    },
    xAxis: {
      type: 'category',
      boundaryGap: false,
      splitLine: {
        show: false,
      },
      axisLabel: {
        textStyle: {
          fontSize: 10,
          fontFamily: 'Montserrat',
          color: '#666',
        },
      },
    },
    yAxis: {
      type: 'value',
      nameLocation: 'end',
      nameGap: '0',
      nameTextStyle: {
        fontFamily: 'Montserrat',
        fontWeight: '500',
        padding: [-25, 0, 0, -48],
        margin: 0,
        align: 'left',
        verticalAlign: 'top',
        width: 'auto',
      },
      scale: true,
      splitNumber: '6',
      axisLabel: {
        margin: 10,
        textStyle: {
          fontSize: 10,
          fontFamily: 'Montserrat',
          color: '#666',
        },
      },
    },
    dataZoom: {
      end: 100,
      borderColor: '#eee',
      fillerColor: 'rgba(0,0,0,.05)',
      textStyle: {
        fontSize: 10,
        fontFamily: 'Montserrat',
        color: '#333',
        textBorderColor: '#fff',
        textBorderWidth: 3,
        fontWeight: '600',
      },
      selectedDataBackground: {
        lineStyle: {
          color: '#333',
        },
        areaStyle: {
          color: '#ffcd00',
        },
      },
      dataBackground: {
        backgroundColor: 'rgba(0,0,0,1)',
        lineStyle: {
          color: '#333',
        },
        areaStyle: {
          color: 'rgba(0,0,0,0.5)',
        },
      },
      handleStyle: {
        color: 'rgba(0,0,0,0.2)',
      },
      moveHandleStyle: {
        color: 'rgba(0,0,0,0.3)',
      },
      emphasis: {
        handleStyle: {
          color: 'rgba(255,205,0,1)',
        },
        moveHandleStyle: {
          color: 'rgba(255,205,0,1)',
        },
      },
    },
    toolbox: {
      feature: {
        restore: { title: 'Restablir' },
        saveAsImage: { title: 'Desar' },
      },
      right: '1%',
      top: '0%',
    },
    series: seriesData,
  };

  const mergedOptions = updateObject(defaultOptions, specificOptions);
  initChart(idChart, mergedOptions);
}
