/**
 * Crear el Evento necesario para que al hacer click sobre una etiqueta HTML cargue la url de un iframe por primera vez.
 * @param {*} clickElementSelector Elemento HTML que provocara la carga del iframe al hacer click sobre el.
 * @param {*} iframeSelector Selector que indica donde esta el iframe al que se le va a cambiar el src.
 * @param {*} url URL que se va a cargar en el iframe.
 */
export function loadIframeOnClickEvent(
  clickElementSelector,
  iframeSelector,
  url,
) {
  const iframe = document.querySelector(iframeSelector);
  const clickElement = document.querySelector(clickElementSelector);

  let loadingIframe = false;
  const loadIframeOnClick = () => {
    if (loadingIframe === false) {
      loadingIframe = true;
      iframe.setAttribute('src', 'about:blank');
      iframe.setAttribute('src', url);
      iframe.onload = () => {
        loadingIframe = false;
      };
    }
  };

  clickElement.addEventListener('click', loadIframeOnClick);
  clickElement.disabled = false;
}
