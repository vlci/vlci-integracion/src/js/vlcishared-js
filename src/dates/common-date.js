const mesDescLarga = {
  '01': 'Enero',
  '02': 'Febrero',
  '03': 'Marzo',
  '04': 'Abril',
  '05': 'Mayo',
  '06': 'Junio',
  '07': 'Julio',
  '08': 'Agosto',
  '09': 'Septiembre',
  10: 'Octubre',
  11: 'Noviembre',
  12: 'Diciembre',
};

/**
 * Dado un valor entero, devuelve el mes/meses asociados a el: 01->Gen
 * @param {object/integer} data Valor a convertir en cadena
 * @returns Cadena asociada al valor.
 */
export function getMonth(data) {
  const monthDict = {
    1: 'Gen',
    2: 'Feb',
    3: 'Mar',
    4: 'Abr',
    5: 'Mai',
    6: 'Jun',
    7: 'Jul',
    8: 'Ago',
    9: 'Sep',
    10: 'Oct',
    11: 'Nov',
    12: 'Des',
  };
  return monthDict[parseInt(data.toString(), 10)];
}

/**
 * Separa un string yyyymm en yyyy & mm
 * @param {string} monthYear
 * @returns
 */
function separateMonthYear(monthYear) {
  return [monthYear.substring(0, 4), mesDescLarga[monthYear.substring(4, 6)]];
}

/**
 * Separa un string yyyymmdd en yyyy mm dd
 * @param {string} date
 * @returns
 */
function separateDayMonthYear(date) {
  return [
    date.substring(0, 4),
    mesDescLarga[date.substring(4, 6)],
    date.substring(6, 8),
  ];
}

/**
 * Dado un valor entero yyyyMM, devuelve el valor del mes + año . Ejem: 202301 --> Enero 2023
 * @param {integer} data Valor a convertir en cadena
 * @returns Cadena asociada al valor.
 */
export function formatMonthAnyo(data) {
  const [anyo, mes] = separateMonthYear(data);
  return `${mes} ${anyo}`;
}

/**
 * Dado un valor entero yyyyMMdd, devuelve el valor del dia mes año . Ejem: 20230101 --> 01 Enero 2023
 * @param {integer} date Valor a convertir en cadena
 * @returns Cadena asociada al valor.
 */
export function formatDayMonthYear(date) {
  const [anyo, mes, dia] = separateDayMonthYear(date);
  return `${dia} ${mes} ${anyo}`;
}

/**
 * Dado un valor entero yyyyMM, devuelve el nombre del mes recortado + año.
 * Ejem: 202301 --> Ene 2023
 * @param {integer} data
 */
export function formatShortMonthYear(data) {
  const [anyo, mes] = separateMonthYear(data);
  return `${mes.substring(0, 3)} ${anyo}`;
}

/**
 * Dado un valor entero yyyyMM, devuelve el nombre del mes recortado + año recortado.
 * Ejem: 202301 --> Ene '23
 * @param {integer} data
 */
export function formatShortMonthShortYear(data) {
  const [anyo, mes] = separateMonthYear(data);
  return `${mes.substring(0, 3)} '${anyo.substring(2, 4)}`;
}

/**
 * Formateo de fecha de un trimestre
 * @param {*} date YYYY-MM
 * @returns trimestre formato MM'T' YYYY
 */
export function formatTrimestre(date) {
  const [anyo, mes] = date.split('-');
  return `${parseFloat(mes) / 3}T ${anyo}`;
}

/**
 * @param MONTHS_YEAR Array con los meses del año
 * @param DEFAULT_LANGUAGE Lenguage por defecto. Ej: es_ES
 * @returns devuelve los 12 meses del año en el idioma que haya por defecto. Solo los 3 primeros caracteres del mes.
 * Por ej: Ene, Feb, ...
 */
export function getMonthList(MONTHS_YEAR, DEFAULT_LANGUAGE) {
  const monthsMap = MONTHS_YEAR[DEFAULT_LANGUAGE];
  const monthNames = Array.from(monthsMap.values()).map((monthName) =>
    monthName.slice(0, 3),
  );
  return monthNames;
}
