import { getWCDFFileNameFromPath } from '../navigation/common-navigation';

/**
 * Función que cambia la clase de la etiqueta Body por una con el nombre del fichero wcdf.
 */
export function changeBodyClass() {
  const bodyElement = document.querySelector('body');
  bodyElement.classList.remove('inicio');
  bodyElement.classList.add(getWCDFFileNameFromPath());
}
