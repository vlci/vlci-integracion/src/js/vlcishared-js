import { changeBodyClass } from '../style/body-class';
import {
  changeTabTitle,
  changeNavTitle,
} from '../navigation/common-navigation';

/**
 * Función que inyecta codigo html de un fichero determinado en una etiqueta html.
 * @param {string} filePath Ruta de fichero que vamos a cargar.
 * @param {element} elementParam Elemento HTML en el que vamos a inyectar la ruta.
 * @param {function} cllbackOnContentLoaded Función que se ejecutará al cargar por completo el fichero HTML en la página.
 */
function includeHTMLFileInElement(
  filePath,
  elementParam,
  cllbackOnContentLoaded,
) {
  const element = elementParam;
  if (filePath) {
    const xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
      if (this.readyState === 4) {
        if (this.status === 200) {
          element.innerHTML = this.responseText;
          if (cllbackOnContentLoaded) {
            cllbackOnContentLoaded();
          }
        }
        if (this.status === 404) {
          element.innerHTML = 'No se ha podido cargar correctamente.';
        }
      }
    };
    xhttp.open('GET', filePath, true);
    xhttp.send();
  }
}

/**
 * Función que inyecta codigo html de un fichero determinado en una serie de etiquetas html
 *  que contienen un atributo en común.
 * @param {string} attrName Nombre del atributo html que se va a buscar entre los elementos.
 * @returns void
 */
export function includeHTMLFileInElements(attrName, cllback) {
  const elements = document.querySelectorAll(`[${attrName}]`);
  elements.forEach((element) => {
    const filePath = element.getAttribute(attrName);
    includeHTMLFileInElement(filePath, element, cllback);
  });
}

/**
 * Función que se ejecuta desde el html de pentaho y que carga diferentes partes de la página.
 * Dentro de ella existen dos funciones que se ejecutan siempre en TODOS LOS PANELES:
 * cabeceraContentLoaded: Esta función se ejecutará cuando se haya cargado la cabecera por completo.
 * mainContentLoaded: Esta función se ejecutará cuando se haya cargado el main por completo incluyendo la función cllback del parametro de esta función.
 * @param {function} cllback Función especifica de un panel que puede ejecutarse trás la carga de la página main.
 * @param {*} id
 */
export function initialCalls(cllback) {
  function cabeceraContentLoaded() {
    changeTabTitle();
    changeNavTitle();
  }

  function mainContentLoaded() {
    document.body.dispatchEvent(new Event('MainContentLoadedCustom'));
    changeBodyClass();
    if (cllback) {
      cllback();
    }
  }
  includeHTMLFileInElements('include-html-cabecera', cabeceraContentLoaded);
  includeHTMLFileInElements('include-html-menu');
  includeHTMLFileInElements('include-html-main-container', mainContentLoaded);
  includeHTMLFileInElements('include-html-copyright');
}
