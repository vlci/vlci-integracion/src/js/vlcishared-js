/**
 * Obtiene una ruta completa del Cdm de Pentaho donde nos encontramos
 * con la ruta relativa proporcionada.
 * @param {string} route Ruta relativa que se concatenara al path principal.
 */
export function calculatePentahoPath(route) {
  document.location = `${document.documentURI.split('%3A')[0]}%3A${
    document.documentURI.split('%3A')[1]
  }%3A${document.documentURI.split('%3A')[2]}%3A${
    document.documentURI.split('%3A')[3]
  }%3A${route}`;
}

/**
 * Función que extrae el nombre del fichero wcdf del path principal.
 * @returns Nombre del fichero wcdf.
 */
export function getWCDFFileNameFromPath() {
  const regExp = /(.*?).wcdf/;
  return document.documentURI.split('%3A')[4].match(regExp)[1];
}

/**
 * Indica si el dispositivo con el que se consulta la pagina es IOS o no.
 * @returns True si es IOS y False si no lo es.
 */
export function isIOSDevice() {
  const { userAgent } = navigator;
  return /iPad|iPhone|iPod|Macintosh/.test(userAgent) && !window.MSStream;
}

export function getEnvironment() {
  const currentUrl = window.location.href;
  let env = '';
  if (currentUrl.includes('localhost')) {
    env = 'local';
  } else if (currentUrl.includes('sc_valencia_pre')) {
    env = 'PRE';
  } else {
    env = 'PRO';
  }
  return env;
}

export function changeTabTitle() {
  const env = getEnvironment();
  if (env !== 'PRO') {
    document.title += ` (${env})`;
  }
}

export function changeNavTitle() {
  const env = getEnvironment();
  if (env !== 'PRO') {
    document.querySelector("nav").querySelectorAll('span')[1].textContent += `    (${env})`;
  }
}