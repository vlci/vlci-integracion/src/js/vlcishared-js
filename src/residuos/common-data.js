export function splitDataset(misDatosArray, isMonth = true) {
  const splittedData = { Cont: [], Mob: [] };
  const COL_CLAS = 0;
  const COL_TIPO = 1;
  const COL_DATO = 2;
  const COL_MES = 3;
  const COL_ANYO = 4;
  const clasCont = 'is-res-001-kgs-contenidors';
  const colTipoTotal = 'total';
  const colTipoMobles = 'Mobles';
  misDatosArray.forEach((datos) => {
    if (datos[COL_CLAS] === clasCont && datos[COL_TIPO] === colTipoTotal) {
      if (isMonth) {
        splittedData.Cont.push([datos[COL_MES], datos[COL_ANYO], datos[COL_DATO]]);
      } else {
        splittedData.Cont.push([datos[COL_ANYO - 1], datos[COL_DATO]]);
      }
    }
    if (datos[COL_TIPO] === colTipoMobles) {
      if (isMonth) {
        splittedData.Mob.push([datos[COL_MES], datos[COL_ANYO], datos[COL_DATO]]);
      } else {
        splittedData.Mob.push([datos[COL_ANYO - 1], datos[COL_DATO]]);
      }
    }
  });
  return splittedData;
}
