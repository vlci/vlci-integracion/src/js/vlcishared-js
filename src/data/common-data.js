/**
 * Separa el conjunto de datos "data" en función de la columna "column"
 * @param {array} data Conjunto de datos a separar y agrupar
 * @param {integer} column Columna por la que se va a agrupar
 * @returns Diccionario con el array "data" agrupado por "column"
 */
export function splitData(data, column) {
  return data.reduce((accumulator, currentValue) => {
    const key = currentValue[column];
    if (!accumulator[key]) {
      accumulator[key] = [];
    }
    accumulator[key].push(currentValue);
    return accumulator;
  }, {});
}

/**
 * Función que partiendo de los datos obtenidos de una query de Pentaho, devuelve el resultset
 * indicando el nombre de las columnas en cada valor
 * @param {Array} data Datos obtenidos de la query de Pentaho.
 * @returns {Array} Array que contiene un objeto por cada fila.
 */
export function resultsetWithFields(data) {
  const { resultset, metadata } = data;
  const resultsetWithFieldsResult = [];
  resultset.forEach((row) => {
    const columnsWithFields = {};
    row.forEach((column, indice) => {
      columnsWithFields[metadata[indice].colName] = column;
    });

    resultsetWithFieldsResult.push(columnsWithFields);
  });
  return resultsetWithFieldsResult;
}
/**
 * Función que actualiza un objeto con otro. Ej)
 
objetoOriginal = {
  propiedad1: {
    atributo1: 'valor1',
    atributo2: 'valor2'
  },
  propiedad2: 'valor2'
};

const objetoParcial = {
  propiedad1: {
    atributo1: 'nuevoValor1'
  }
};

objetoActualizado = {
  propiedad1: {
    atributo1: 'nuevoValor1',
    atributo2: 'valor2'
  },
  propiedad2: 'valor2'
};
 * @param {Object} original Objeto original que quiere ser actualizado. 
 * @param {Object} modified Objeto que contiene los valores ESPECIFICOS que queremos actualizar.
 * @returns 
 */
export function updateObject(original, modified) {
  const originalOut = { ...original };

  Object.keys(modified).forEach((key) => {
    // Evita los atributos heredados que no hemos creado nosotros.
    if (Object.prototype.hasOwnProperty.call(modified, key)) {
      if (key === 'series' && Array.isArray(modified[key])) {
        originalOut[key] = modified[key].map((specificSeries, index) => {
          const originalSeries = Array.isArray(original[key])
            ? original[key][index] || {}
            : {};
          return { ...originalSeries, ...specificSeries };
        });
      }
      // Comprueba si el valor de la propiedad es un objeto y no un array
      else if (
        typeof modified[key] === 'object' &&
        modified[key] !== null &&
        !Array.isArray(modified[key])
      ) {
        // Si la propiedad es un objeto, llama recursivamente a la función para combinar sus atributos
        originalOut[key] = updateObject(original[key] || {}, modified[key]);
      } else if (original[key] !== modified[key]) {
        originalOut[key] = modified[key];
      }
    }
  });
  // Devuelve el objeto original actualizado
  return originalOut;
}

/**
 * Pinta los datos obtenidos por una query de pentaho en el HTML aplicando una configuración.
 * @param {Object} data Objeto que devuelve pentaho tras realizar una query que contiene los datos que se van a pintar.
 * Requisitos:
 * 1- Tener una columna con el nombre id y coincida con el id del atributo del dataConfig.
 * 2- Cada fila del objeto debe tener un id distinto.
 * @param {Object} dataConfig Objeto que contiene la configuración de los datos que se van a pintar. Lo que no este declarado en este objeto, no se pintará.
 * Requisitos:
 * 1- El objeto contendra una lista de elementos que se corresponderan con el id de cada fila del objeto data.
 * 2- Dentro de cada elemento crearemos un nuevo atributo con el nombre de la columna que queremos pintar. Este atributo puede configurarse de dos formas distintas:
 *  2.1- Se puede asignar un valor directamente que correspondera con el selector del elemento html donde queremos pintarlo.
 *  2.2- Se puede crear un objeto que contendra dos elementos más:
 *    2.2.1- element contendra el selector del elemento html donde queremos pintarlo.
 *    2.2.2- format contendra una funcion con la que se podra formatear el valor antes de pintarlo.
 * 2.3 - Se puede crear un objeto que renombre el nombre de la clase por uno que venga de base de datos añadiendo un elemento className
 * Ejemplo:
 * const dataConfig = {
    NumeroQuejasSugerenciasMes: {                 <--- Id que debe coincidir con la columna id del data.
      value: '#indQuejasVal',                     <--- La columna value del data será pintada en el selector HTML indicado.
      variacion: {
        element: '#indQuejasVar',                 <--- La columna variacion del data será pintada en el selector HTML indicado.
        format: (value) => `${value}%`,           <--- Además se le aplicará el formato indicado aqui antes de ser insertado.
      },
      tendencia: {                                <--- Id que debe coincidir con la columna que devuelve el valor por el que vamos a renombrar la clase
        className: '.prePrePrevioTrim',           <--- El valor del elemento className será el de la clase indicada en el HTML.
      },
    RendimientoAdministrativo:{....}
    }
 */
export function printDataInHTMLElements(data, dataConfig) {
  console.log('INICIO HTML');
  const updateElements = () => {
    data.forEach((indicador) => {
      if (indicador.id in dataConfig) {
        Object.entries(dataConfig[indicador.id]).forEach((elementArray) => {
          const key = elementArray[0];
          const value = elementArray[1];
          if (typeof value === 'object') {
            if (value.className) {
              document.querySelector(value.className).className =
                indicador[key];
            } else if (value.element && value.format) {
              document.querySelector(value.element).textContent = dataConfig[
                indicador.id
              ][key].format(indicador[key]);
            }
          } else {
            document.querySelector(value).textContent = indicador[key];
          }
        });
      }
    });
  };

  try {
    updateElements();
  } catch (e) {
    // Wait for the custom event to ensure elements are available
    document.body.addEventListener('MainContentLoadedCustom', () => {
      updateElements();
    });
  }
}

/**
 * Función que devuelve el máximo de uno de los campos del objeto data recuperado en una query.
 * @param {Object} data Objeto que contiene los datos de la query.
 * @param {String} fieldName Nombre del campo del cual queremos obtener el maximo.
 * @returns Si existia al menos un número en el campo especificado, devolvera el maximo del mismo, en caso contrario devuelve null.
 */
export function getMaxFieldNumberFromData(data, fieldName) {
  const resultSet = resultsetWithFields(data);
  const array = [];
  let max = null;
  resultSet.forEach((element) => {
    const fieldValue = element[fieldName];
    if (typeof fieldValue === 'number' && !Number.isNaN(fieldValue)) {
      array.push(fieldValue);
    }
  });

  if (array.length > 0) {
    max = Math.max(...array);
  }
  return max;
}

/**
 * Inserta el contenido en un elemento del DOM, pasandole el id del elemento (selector) y el valor que queremos insertar.
 * @param {string} selector - El selector CSS del elemento (e.g., '#consumoTotal').
 * @param {number|string} value - El valor a inyectar en el elemento.
 * @param {function} [formatter] - (Opcional) Una función para formatear el valor antes de inyectarlo.
 */
export function updateDomElement(selector, value) {
  const domElement = document.querySelector(selector);
  if (domElement) {
    domElement.textContent = value;
  } else {
    console.warn(`Elemento ${selector} no encontrado en el DOM`);
  }
}