import { splitData, updateObject } from './common-data';
import { getMonth } from '../dates/common-date';
import { addPointInteger } from '../formatting/common-number';
import { formatName } from '../formatting/common-text';
import { initChart, formatTooltip } from '../charts/common-chart';

/**
 * Método que devuelve el máximo de una columna en concreto de un array.
 * @param {array} array Array que contiene la columna a tratar.
 * @param {integer} column Indice de la columna.
 * @param {float} add Si se quiere incrementar el máximo en X unidades.
 * @returns el valor máximo de array en la columna column
 */
function getMaxGaugeValue(array, column, add = 1) {
  const max = Math.max(...array.map((row) => row[column])) * add;
  return Math.ceil(max / 5000) * 5000;
}

/**
 * Configura los datos de los Gauge Anuales o multiserie.
 * @param {Text} idChart Identificador del Chart
 * @param {Array} misDatosArray Resultset obtenido de la query a la BBDD
 * @param {Object} objColData Objeto que indica la correspondencia
 * entre la posicion y la columna de misDatosArray.
 * @param {Array} colorsDataGauge Array que contiene los colores que se van a utilizar.
 * @param {Number} fontSize Tamaño de la fuente utilizado.
 * @param {Number} paramOffsetCenterTitle Porcentaje de la ubicacion vertical
 * del titulo de las series.
 * @param {Number} paramOffsetCenterDetail Porcentaje de la ubicacion vertical
 * del valor de las series.
 * @param {Number} paramOffsetLabel Porcetaje de la ubicación horizontal desde el centro
 * tanto para el titulo como para el valor de las series.
 * @param {Number} paramCalculatedOffset Diferencia de porcentaje de la ubicación horizontal
 * entre una serie y otra.
 * @param {Number} paramNumberOfElementsByLine Numero de series que se van a mostrar por cada fila.
 * @returns Devuelve el valor maximo del gauge y los datos. */

export function configureDataGaugeCommon(
  misDatosArray,
  objColData,
  colorsDataGauge,
  fontSize = 30,
  paramOffsetCenterTitle = 15,
  paramOffsetCenterDetail = 30,
  paramOffsetLabel = -70,
  paramCalculatedOffset = 70,
  paramNumberOfElementsByLine = 3,
) {
  const maxGaugeValue = getMaxGaugeValue(misDatosArray, objColData.data, 1.1); // Valor maximo del gauge
  const calculatedOffset = paramCalculatedOffset;
  const colorsCopy = colorsDataGauge.slice();
  const gaugeDataYears = [];
  let offsetCenterTitle = paramOffsetCenterTitle;
  let offsetCenterDetail = paramOffsetCenterDetail;
  let offsetLabel = paramOffsetLabel;

  for (let i = 0; i < misDatosArray.length; i += 1) {
    gaugeDataYears.push({
      value: misDatosArray[i][objColData.data],
      name: formatName(
        String(misDatosArray[i][objColData.type]).replace(/\[.*?\]/g, ''),
      ),

      color: colorsCopy.shift(),
      title: {
        offsetCenter: [
          `${String(offsetLabel)}%`,
          `${String(offsetCenterTitle)}%`,
        ],
        fontFamily: 'Montserrat',
        fontSize,
      },
      detail: {
        offsetCenter: [
          `${String(offsetLabel)}%`,
          `${String(offsetCenterDetail)}%`,
        ],
        fontFamily: 'Montserrat',
        width: '33%',
        formatter: (value) => addPointInteger(Math.round(value)),
      },
    });
    offsetLabel += calculatedOffset;
    if ((i + 1) % paramNumberOfElementsByLine === 0) {
      offsetCenterDetail += paramOffsetCenterDetail;
      offsetCenterTitle += paramOffsetCenterTitle * 2;
      offsetLabel = paramOffsetLabel;
    }
  }

  return [maxGaugeValue, gaugeDataYears];
}

/**
 * Modulo que formatea en funcion del tamaño de la pantalla
 * @param {array} misDatosArray Array con los datos a formatear
 * @param {*} value Valor del campo que está tratando
 * @param {Number} index Indica la posicion del dato a representar dentro de misDatosArray
 * @returns cadena formateada segun las dimensiones de la pantalla
 */
export function labelFormatterMonthlyDataGauge(misDatosArray, value, index) {
  const valor = addPointInteger(Math.round(value));
  const month = getMonth(misDatosArray[index][0]);
  const year = misDatosArray[index][1];
  const textBelowGraph = `\n${month} ${year} : ${valor}`;
  const textIntoGraph = `${valor}\n${month} ${year}`;
  return window.innerWidth <= 1710 ? textBelowGraph : textIntoGraph;
}

/**
 * Configura los datos de los Gauge Mensuales o de una sola serie.
 * @param {Text} idChart Identificador del Chart
 * @param {Array} misDatosArray Resultset obtenido de la query a la BBDD
 * @param {Object} objColData Objeto que indica la correspondencia
 * entre la posicion y la columna de misDatosArray.
 * @param {Number} monthNumber Indica el numero de mes que vamos a representar desde el actual,
 * siendo 0 el mes actual, 1 el anterior, etc.
 * @returns {Array} Devuelve el valor maximo del gauge,
 *  el formato del detalle del Gauge y los datos. */

export function configureDataGaugeMonthCommon(
  idChart,
  misDatosArray,
  objColData,
  monthNumber = 0,
) {
  const maxGaugeValue = getMaxGaugeValue(misDatosArray, objColData.data, 1);
  const seriesDetailFormatter = (value) =>
    labelFormatterMonthlyDataGauge(misDatosArray, value, monthNumber);
  const gaugeDataMonth = [
    {
      value: misDatosArray[monthNumber][objColData.data],
    },
  ];
  return [maxGaugeValue, seriesDetailFormatter, gaugeDataMonth];
}

/**
 * Configura un gráfico que tiene varias series
 * @param {Array} datos datos iniciales.
 * @param {boolean} round indica si se quiere redondear los datos de entrada.
 * @returns Datos configurados.
 */
export function configureMultipleSeries(datos, round) {
  const { resultset } = { resultset: datos };
  const tipos = new Set();
  const valueAxisX = new Set();

  const dataObj = resultset.map(([tipo, fecha, dato]) => {
    const val = round ? Math.round(dato) : dato;
    tipos.add(tipo);
    valueAxisX.add(fecha);
    return { day: fecha, type: tipo, value: val };
  });

  const axisXArray = Array.from(valueAxisX.values());
  const tiposArray = Array.from(tipos.values());
  // Ordenar si los nombres vienen desordenados:
  tiposArray.sort((a, b) =>
    a
      .toLowerCase()
      .localeCompare(b.toLowerCase(), 'es', { ignorePunctuation: true }),
  );

  const series = tiposArray.map((tipo) => {
    const arrayData = dataObj
      .filter((obj) => obj.type === tipo)
      .map(({ day, value }) => [day, tipo, value]);

    const arrayDataClean = axisXArray.map((fecha) => {
      const obj = arrayData.find(([d]) => d === fecha);
      return obj ? obj[2] : null;
    });
    return {
      name: tipo,
      data: arrayDataClean,
    };
  });

  return [series, tiposArray, valueAxisX];
}

/**
 * Configura los datos para los gráficos de líneas.
 * @param {Array} datos datos iniciales.
 * @param {Array} color colores de las lineas a mostrar en la gráfica.
 * @param {boolean} round indica si se quiere redondear los datos de entrada.
 * @returns Datos configurados.
 */
export function configureDataLine(datos, colors = [], round = false) {
  const [series, tiposArray, valueAxisX] = configureMultipleSeries(
    datos,
    round,
  );
  // Configurar el tipo de línea para cada serie individual
  series.forEach((serie) => {
    serie.type = 'line';
    serie.symbol = 'circle';
    serie.symbolSize = 6;
    serie.smooth = 0.1;
    if (colors.length > 0) {
      serie.color = colors.shift();
    }
  });

  return [series, valueAxisX, tiposArray];
}

/**
 * Configura los datos para los gráficos de barras.
 * @param {Array} datos datos iniciales.
 * @param {Array} color colores de las lineas a mostrar en la gráfica.
 * @param {boolean} round indica si se quiere redondear los datos de entrada.
 * @returns Datos configurados.
 */
export function configureDataBar(datos, colors, round = false) {
  const [series, tiposArray, valueAxisX] = configureMultipleSeries(
    datos,
    round,
  );
  // Configurar el tipo de línea para cada serie individual
  series.forEach((serie) => {
    serie.type = 'bar';
    serie.color = colors.shift();
    serie.smooth = false;
  });

  return [series, valueAxisX, tiposArray];
}
