import { isIOSDevice } from '../navigation/common-navigation';

export function buildCamarasTrafico(
  paramIframeAccesosCiudad,
  paramIframeViasPrincipales,
  paramSectionContainer,
  paramDivCamaras,
  paramBlurButton,
  paramMillisBeforeBlur,
) {
  const URL_CAMARA_ACCESO_CIUDAD_CAS =
    'https://camaras.valencia.es/camaras/ocimovil.html?camara=1&lang=cas';
  const URL_CAMARA_VIAS_PRINCIPALES_CAS =
    'https://camaras.valencia.es/camaras/ocimovil.html?lang=cas';
  const URL_CAMARA_ACCESO_CIUDAD_VAL =
    'https://camaras.valencia.es/camaras/ocimovil.html?camara=1&lang=val';
  const URL_CAMARA_VIAS_PRINCIPALES_VAL =
    'https://camaras.valencia.es/camaras/ocimovil.html?lang=val';

  const COD_ISO_VALENCIA = 'ca-ES';
  let firstAccess = false;
  let blurModeActive = false;

  const iframeAccesosCiudad = document.querySelector(paramIframeAccesosCiudad);
  const iframeViasPrincipales = document.querySelector(
    paramIframeViasPrincipales,
  );

  /**
   * Agregar al Iframe la url de las camaras de trafico
   */
  const refreshCamaras = () => {
    if (
      typeof Liferay !== 'undefined' &&
      // eslint-disable-next-line no-undef
      Liferay.ThemeDisplay.getBCP47LanguageId() === COD_ISO_VALENCIA
    ) {
      if (iframeAccesosCiudad) {
        iframeAccesosCiudad.setAttribute('src', URL_CAMARA_ACCESO_CIUDAD_CAS);
      }
      if (iframeViasPrincipales) {
        iframeViasPrincipales.setAttribute(
          'src',
          URL_CAMARA_VIAS_PRINCIPALES_CAS,
        );
      }
    } else {
      if (iframeAccesosCiudad) {
        iframeAccesosCiudad.setAttribute('src', URL_CAMARA_ACCESO_CIUDAD_VAL);
      }
      if (iframeViasPrincipales) {
        iframeViasPrincipales.setAttribute(
          'src',
          URL_CAMARA_VIAS_PRINCIPALES_VAL,
        );
      }
    }
  };

  /**
   * Quita las url de las camaras de los iframes
   */
  const stopCamaras = () => {
    if (iframeAccesosCiudad) {
      iframeAccesosCiudad.src = 'about:blank';
    }
    if (iframeViasPrincipales) {
      iframeViasPrincipales.src = 'about:blank';
    }
  };

  /**
   * Oculta toda la caja de camaras.
   */
  function hideCamaras() {
    const divCamaras = document.querySelectorAll(paramDivCamaras)[0];
    divCamaras.style.display = 'none';
  }

  /**
   * Permite activar el modo blur que muestra las camaras difuminadas con un boton de recarga.
   * @param {boolean} active true para activar, false para desactivar.
   */
  function blurMode(active = true) {
    const sectionContainer = document.querySelector(paramSectionContainer);
    if (active) {
      stopCamaras();
      sectionContainer.classList.add('blur');
      blurModeActive = true;
    } else {
      sectionContainer.classList.remove('blur');
      refreshCamaras();
      blurModeActive = false;
    }
  }

  /**
   * Evento que activa la visualizacion de las camaras si se muestra
   * por pantalla.
   * @param {Object} iframe
   * @param {String} urlCamarasCas
   * @param {String} urlCamarasVal
   */
  const handlerIsVisibleIframe = (iframe) => {
    const observerCamara = new IntersectionObserver((entries) => {
      entries.forEach((entry) => {
        if (entry.isIntersecting) {
          if (!blurModeActive) {
            refreshCamaras();
          }
          // Aqui solo entra la primera vez
          if (!firstAccess) {
            firstAccess = true;
            window.setTimeout(blurMode, paramMillisBeforeBlur);
          }
        }
      });
    });
    observerCamara.observe(iframe);
  };

  function linksCamarasTrafico() {
    if (!isIOSDevice()) {
      if (iframeAccesosCiudad) {
        iframeAccesosCiudad.setAttribute('sandbox', 'allow-scripts');
        handlerIsVisibleIframe(iframeAccesosCiudad);
      }
      if (iframeViasPrincipales) {
        iframeViasPrincipales.setAttribute('sandbox', 'allow-scripts');
        handlerIsVisibleIframe(iframeViasPrincipales);
      }
      document.querySelector(paramBlurButton).addEventListener('click', () => {
        blurMode(false);
        window.setTimeout(blurMode, paramMillisBeforeBlur);
      });
    } else {
      hideCamaras();
    }
  }
  linksCamarasTrafico();
}
