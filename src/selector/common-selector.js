const monthNamesEs = [
  'Enero',
  'Febrero',
  'Marzo',
  'Abril',
  'Mayo',
  'Junio',
  'Julio',
  'Agosto',
  'Septiembre',
  'Octubre',
  'Noviembre',
  'Diciembre',
];

const abbreviatedMonthNamesEs = [
  'Ene',
  'Feb',
  'Mar',
  'Abr',
  'May',
  'Jun',
  'Jul',
  'Ago',
  'Sep',
  'Oct',
  'Nov',
  'Dic',
];
const dayNamesEs = [
  'Domingo',
  'Lunes',
  'Martes',
  'Miércoles',
  'Jueves',
  'Viernes',
  'Sábado',
];

const abbreviatedDayNamesEs = ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'];
const dayNamesMinEs = ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'];

/**
 * Por defecto provoca que el filtro seleccione todos los filtros
 * @param {Array} data Elementos del filtro
 * @returns data Elementos del filtro
 */
export function selectFilterAll(data) {
  const selectedItems = data.resultset.map((item) => item[0]);
  this.dashboard.fireChange(this.parameter, selectedItems);
  return data;
}

/**
 * Comprueba si los elementos de un array son todos 'All', incluso un array dentro otro array
 * @param {Array} arr Array de entrada
 * @returns true si todos los elementos son 'All'
 */
function isAll(arr) {
  for (const element of arr) {
    if (Array.isArray(element)) {
      if (!isAll(element)) {
        return false;
      }
    } else if (element !== 'All') {
      return false;
    }
  }
  return true;
}

/**
 * Selecciona el datasource que se debe ejecutar en los filtros y modificar parámetros.
 * En la primera ejecución se cargarán todos los elementos en los filtros y el valor del parámetro
 * se pondrá a 'All' para la correcta ejecución de las gráficas que están escuchando
 * @param {FilterComponent} filterOrigen componentes del filtro
 * @param {Array} filterParam Array con los valores del resto de filtros relacionados con este
 * @param {Array} queryDatasourceLoad datasource que se ejecutará al cargar la página o resetear
 * @param {Array} queryDatasource nombre del datasource que se ejecutará el resto de veces
 * @returns true Si todo funciona correctamente
 */
export function selectConfigDatasource(
  filterOrigen,
  filterParam,
  queryDatasourceLoad,
  queryDatasource,
) {
  // Comprobar si estamos en la primera carga de los datos
  if (isAll(filterParam)) {
    filterOrigen.queryDefinition.dataAccessId = queryDatasourceLoad;
    filterOrigen.dashboard.setParameter(filterOrigen.parameter, ['All']);
  } else {
    filterOrigen.queryDefinition.dataAccessId = queryDatasource;
  }
  return true;
}
/**
 * Esta función configura un filtro o parámetro en un dashboard (tablero) para seleccionar 
 * todos los valores posibles para un filtro específico (lo cual es indicado por ['All']).
 * @param {FilterComponent} filterOrigen componentes del filtro
 * @returns true
 */
export function selectConfigDatasourceAllValue(
  filterOrigen
) {  
    filterOrigen.dashboard.setParameter(filterOrigen.parameter, ['All']);  
  return true;
}

/**
 * Resetea los filtros de todos los selectores, excepto los de fechas
 * @param {Array} filterParam Array con los parámetros de los filtros
 */
export function resetFilter(filterParam) {
  filterParam.forEach((element) => {
    Dashboards.fireChange(element, ['All']);
  });
}

/**
 * Configura los componentes de filtros
 * @param {string} name nombre que aparecerá en el filtro, por defecto es 'Todos' 
 * @returns configuracion del filtro
 */
export function getSelectCommonOptions(name = "Todos") {
  return {
    input: {
      root: {
        id: 'All',
      },
      indexes: {
        id: 0,
        label: 0,
      },
    },
    component: {
      Root: {
        strings: {
          isDisabled: 'No disponible',
          allItems: name,
          noItems: 'Ninguno',
          btnApply: 'Aceptar',
          btnCancel: 'Cancelar',
        },
      },
      Group: {
        Strings: {
          allItems: 'Todos',
          noItems: 'Ninguno',
          btnApply: 'Aceptar',
          btnCancel: 'Cancelar',
        },
      },
    },
  };
}

/**
 * Configura los componentes de filtros que van a recibir una query
 * donde la primera columna es el label que se vera en el cuadro de mandos y la segunda columna es el valor que tendrá el filtro
 * @param {string} name nombre que aparecerá en el filtro, por defecto es 'Todos' 
 * @returns configuracion del filtro
 */
export function getSelectTwoColumnsOptions(name = "Todos") {
  const idLabelOptions = getSelectCommonOptions(name);
  idLabelOptions.input.indexes.id = 1;
  idLabelOptions.input.indexes.label = 0;
  return idLabelOptions;
}

/**
 * Resetea el filtro de fechas, asignando los valores de las fechas por defecto
 * @param {String} paramameterResetIni parametro que se reseteará con el valor por defecto de la fecha de inicio
 * @param {String} paramameterResetFin parametro que se reseteará con el valor por defecto de la fecha de fin
 * @param {String} paramameterLoadIni parametro que contiene el valor por defecto de la fecha de inicio
 * @param {String} paramameterLoadFin parametro que contiene el valor por defecto de la fecha de fin
 * @returns true Si todo funciona correctamente
 */
export function resetFilterDates(
  paramameterResetIni,
  paramameterResetFin,
  paramameterLoadIni,
  paramameterLoadFin,
) {
  const valueIni = Dashboards.getParameterValue(paramameterLoadIni);
  const valueFin = Dashboards.getParameterValue(paramameterLoadFin);
  Dashboards.fireChange(paramameterResetIni, [valueIni]);
  Dashboards.fireChange(paramameterResetFin, [valueFin]);

  return true;
}

/**
 * Resetea un filtro simple de fecha, asignando el valor de fecha por defecto
 * @param {String} paramameterResetIni parametro que se reseteará con el valor por defecto de la fecha de inicio
 * @param {String} paramameterLoadIni parametro que contiene el valor por defecto de la fecha de inicio */
export function resetFilterDate(
  paramameterResetIni,
  paramameterLoadIni
) {
  const valueIni = Dashboards.getParameterValue(paramameterLoadIni);
  Dashboards.fireChange(paramameterResetIni, [valueIni]);
  return true;
}

/**
 * Inicializa el parametro que contiene el valor de fecha por defecto
 * @param {*} data es el dato de la query que se usa para obtener el valor por defecto
 * @param {String} filterParam  parámetro que contiene el valor de fecha por defecto
 */
export function initValueDefaultDate(data, filterParam) {
  Dashboards.fireChange(filterParam, data.resultset[0][0]);
}

/**
 * Selecciona el valor de fecha por defecto en el selector de fecha
 * @param {*} filterComponent
 * @param {String} parameterOrigin parámetro que contiene el valor de fecha por defecto
 */
export function selectValueDefaultInFilter(filterComponent, parameterOrigin) {
  const paramDefault = Dashboards.getParameterValue(parameterOrigin);
  const paramFilter = filterComponent.parameter;
  filterComponent.dashboard.setParameter(paramFilter, [paramDefault]);
}

/**
 * Poner en castellano los meses y días de las semana del calendaria que muestra el selector de fechas
 * @param {datepicker} datepicker Componente datepciker del que se modificarán sus propiedades
 */
export function convertToLanguageSelectorDateRange(datepicker) {
  datepicker.regional.es = {
    monthNames: monthNamesEs,
    abbreviatedMonthNames: abbreviatedMonthNamesEs,
    dayNames: dayNamesEs,
    abbreviatedDayNames: abbreviatedDayNamesEs,
    dayNamesMin: dayNamesMinEs,
    weekHeader: 'Sm',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: '',
  };
  datepicker.setDefaults(datepicker.regional.es);
}

/**
 * Resetea un filtro a su valor por defecto
 * @param {String} parameterReset El parámetro que se reseteará
 * @param {String|Array} parameterDefault El valor por defecto que se asignará al filtro
 * @returns true Si todo funciona correctamente
 */
export function resetFilterToDefault(parameterReset, parameterDefault) {
  const defaultValue = Dashboards.getParameterValue(parameterDefault);
  Dashboards.fireChange(parameterReset, Array.isArray(defaultValue) ? defaultValue : [defaultValue]);
  return true;
}