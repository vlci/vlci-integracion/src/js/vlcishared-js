import { THRESHOLDS } from '../constants/constants';

export function buildTableCalidadAire(resultset) {
  const trArray = [];
  const CSS_CLASS_THRESHOLD = {
    0: 'b',
    1: 'a',
    2: 'd',
    3: 'm',
    4: 'mm',
    5: 'em',
    6: '',
    7: 'nd',
  };
  /**
   * Setea la clase CSS al objeto con el id que recibe por parámetro
   * dependiendo de en que umbral se encuentra el valor.
   * @param {Array} array
   * @param {Number} value
   * @param {String} idCss
   * @returns {String}
   */
  function getCssClass(array, value) {
    let threshold;
    if (value === undefined) {
      threshold = 6;
    } else if (value === 'ND') {
      threshold = 7;
    } else {
      threshold = 5;
      array.forEach((element, index) => {
        if (value <= element && threshold === 5) {
          threshold = index;
        }
      });
    }
    return CSS_CLASS_THRESHOLD[threshold];
  }

  /**
   * Crea un elemento td para una tabla html con un id y valor determinados.
   * @param {string} idName Id que se le asignará al td.
   * @param {string} value  Valor del td.
   * @param {string} threshold  Array que contenga los umbrales.
   * @returns {td node} Elemento td
   */
  function createTdElement(idName, value, threshold) {
    const tdElement = document.createElement('td');
    tdElement.setAttribute('id', idName);
    if (value !== undefined && value !== null) {
      tdElement.innerHTML = value;
      const cssClass = getCssClass(threshold, value);
      tdElement.classList.add(cssClass);
    }
    return tdElement;
  }

  resultset.forEach((row) => {
    const estacion = row;
    // Si los datos estan desactualizados mostramos N/D en la estación
    if (estacion.operationalstatus === 'noData') {
      estacion.so2value = 'ND';
      estacion.no2value = 'ND';
      estacion.o3value = 'ND';
      estacion.pm10value = 'ND';
      estacion.pm25value = 'ND';
    }

    // ELIMINAR PARA MOSTRAR DATOS PM10 Y PM25 DE CABANYAL
    if (estacion.entityid === 'A09_CABANYAL_60m') {
      estacion.pm10value = undefined;
      estacion.pm25value = undefined;
    }
    // Rellenando la tabla dinamica
    const markupTrElement = document.createElement('tr');
    const markupTdElement = document.createElement('td');
    markupTdElement.innerHTML = estacion.address;
    markupTrElement.appendChild(markupTdElement);

    markupTrElement.appendChild(
      createTdElement(
        `${estacion.entityid}_so2`,
        estacion.so2value,
        THRESHOLDS.SO2,
      ),
    );
    markupTrElement.appendChild(
      createTdElement(
        `${estacion.entityid}_no2`,
        estacion.no2value,
        THRESHOLDS.NO2,
      ),
    );
    markupTrElement.appendChild(
      createTdElement(
        `${estacion.entityid}_o3`,
        estacion.o3value,
        THRESHOLDS.O3,
      ),
    );
    markupTrElement.appendChild(
      createTdElement(
        `${estacion.entityid}_pm10`,
        estacion.pm10value,
        THRESHOLDS.PM10,
      ),
    );
    markupTrElement.appendChild(
      createTdElement(
        `${estacion.entityid}_pm25`,
        estacion.pm25value,
        THRESHOLDS.PM25,
      ),
    );
    trArray.push(markupTrElement);
  });

  return trArray;
}
