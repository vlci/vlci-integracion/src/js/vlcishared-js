import { initElement } from '../charts/common-chart';

/**
 * Inicializa la tabla con las opciones indicadas
 * @param {String} tableId Nombre del html.
 * @param {String} option opciones de la tabla.
 * @returns tabla completa.
 */
export function initTable(tableId, option) {
  const initializeTable = (table) => new Tabulator(table, option);

  return initElement(tableId, initializeTable);
}

/**
 * Separa las cadenas para su correcta representición.
 * @param {String} name Nombre a formatear.
 * @param {Integer} size numero de caracteres que como máximo se mostrarán en cada fila.
 * @returns Cadena formateada.
 */
export function formatNameHeaderColumn(cell, size) {
  let columnName = cell.getValue();
  if (columnName.length > size) {
    columnName = `${columnName.substr(0, size)}<br>${columnName.substr(size)}`;
  }
  return columnName;
}

/**
 * Separa en diferentes líneas los valores de las celdas.
 * @param {String} cell Valor de la celda.
 * @param {Integer} size numero de caracteres que como máximo se mostrarán en cada fila.
 * @returns Cadena formateada.
 */
export function formatCellMultiplesLines(cell, size) {
  const columnName = cell.getValue();
  const words = columnName.split(' '); // Se divide el contenido del a celda en palabras
  let lines = '';
  let currentLine = '';
  // Recorrer las palabras y construir líneas
  words.forEach((word) => {
    if ((currentLine + word).length <= size) {
      currentLine += `${word} `;
    } else {
      lines += `${currentLine}<br>`;
      currentLine = `${word} `;
    }
  });
  lines += `${currentLine}<br>`;

  return lines;
}

/**
 * Expande o contrae todos los nodos de tree view.
 * @param {Row} row Nodo del tree view.
 * @param {String} action Indica la acción a realizar. Si action es igual a 'expand', la función expandirá el nodo; de lo contrario, la contraerá.
 */
function findChildren(row, action) {
  if (action === 'expand') {
    row.treeExpand();
  } else {
    row.treeCollapse();
  }

  const childRows = row.getTreeChildren();

  if (childRows.length > 0) {
    childRows.forEach((child) => {
      if (child.getTreeChildren().length > 0) {
        findChildren(child, action);
      }
    });
  }
}

/**
 * Recorre todas las filas de una tabla tree view y aplica la función findChildren a cada una de ellas.
 * @param {Table} tbl Representa una tabla con filas y estructura tree view.
 * @param {String} action Indica la acción a realizar. Si action es igual a 'expand', la función expandirá el nodo; de lo contrario, la contraerá.
 */
export function traverseRows(tbl, action) {
  const tblRows = tbl.getRows();
  tblRows.forEach((row) => {
    if (row.getTreeChildren().length > 0) {
      findChildren(row, action);
    }
  });
}

/**
 * Convierte la estructura de una tabla a tree view.
 * @param {Array} origen Contiene el array de elemento origen devueltos por la query
 * @param {String} nodeParent Elemento padre del treeView
 * @param {Array} listNameChildren Lista de nombres de los elementos hijos
 * @param {String} valueChildren Elemento hijo del treeView
 */
export function convertToTableTreeView(
  origen,
  nodeParent,
  listNameChildren,
  valueChildren,
) {
  const dataObj = [];

  for (const key in origen) {
    if (origen.hasOwnProperty(key)) {
      const subArray = origen[key];

      const areaObj = {
        [nodeParent]: subArray[0],
        _children: [],
      };

      subArray.shift(); // Elimina el primer elemento del array, en este caso el área
      subArray.forEach((valor, index) => {
        const childObj = {
          [nodeParent]: listNameChildren[index],
          [valueChildren]: valor,
        };

        areaObj._children.push(childObj);
      });

      dataObj.push(areaObj);
    }
  }

  return dataObj;
}

/**
 * Verifica si todas las agrupaciones en la tabla están colapsadas.
 * @returns {boolean} Devuelve true si están todas las agrupaciones colapsadas y false si no.
 */
export function checkAllGroupsCollapsed(table) {
  let allCollapsed = true;
  const groups = table.getGroups();
  groups.forEach((group) => {
    if (group._group.visible) {
      allCollapsed = false;
    }
  });
  return allCollapsed;
}

/**
 * Exporta en CSV la tabla.
 * @param {Table} table elemento tabla del que se descargá la información
 * @param {String} nombreFichero nombre del fichero que se descargará (sin la extensión)
 * @param {String} elementHTML nombre el elemento HTML
 */
export function downloadCSVTable(table, nombreFichero, elementHTML) {
  const previousClickHandler = () => {
    table.download('csv', `${nombreFichero}.csv`, { delimiter: ';' });
  };
  const downloadCsvButton = document.getElementById(elementHTML);

  const newDownloadCsvButton = downloadCsvButton.cloneNode(true);
  downloadCsvButton.parentNode.replaceChild(
    newDownloadCsvButton,
    downloadCsvButton,
  );

  newDownloadCsvButton.addEventListener('click', previousClickHandler);
}

/**
 * Exporta en PDF la tabla.
 * @param {Table} table elemento tabla del que se descargá la información
 * @param {String} nombreFichero nombre del fichero que se descargará (sin la extensión)
 */
export function downloadPDFTable(table, nombreFichero, elementHTML) {
  const previousClickHandler = () => {
    table.download('pdf', `${nombreFichero}.pdf`, {
      title: nombreFichero,
      rowCalcStyles: {
        fontSize: 8,
        fontStyle: 'bold',
      },
      autoTable: {
        styles: {
          fontSize: 8,
        },
      },
    });
  };
  const downloadCsvButton = document.getElementById(elementHTML);

  const newDownloadPdfButton = downloadCsvButton.cloneNode(true);
  downloadCsvButton.parentNode.replaceChild(
    newDownloadPdfButton,
    downloadCsvButton,
  );

  newDownloadPdfButton.addEventListener('click', previousClickHandler);
}
