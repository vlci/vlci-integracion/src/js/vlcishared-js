const path = require('path');
const webpack = require('webpack');

module.exports = {
  devServer: {
    port: 8889,
    client: {
      overlay: false,
    },
  },
  devtool: 'eval-cheap-module-source-map',
  mode: 'development',
  optimization: {
    usedExports: true,
    minimize: true,
  },
  entry: {
    vlcishared: './src/index.js',
  },
  output: {
    path: path.resolve(__dirname, 'dist/js'),
    filename: '[name].js',
    library: '[name]',
    libraryTarget: 'umd',
    clean: true,
    publicPath: '/dist/js/',
  },
  plugins: [
    new webpack.ProvidePlugin({
      Buffer: ['buffer', 'Buffer'],
    }),
    new webpack.ProvidePlugin({
      process: 'process/browser',
    }),
  ],
  externals: {
    echarts: 'echarts',
    'tabulator-tables': 'tabulator-tables',
  },
};
