const path = require('path');

const webpack = require('webpack');

module.exports = {
  devtool: 'source-map',
  mode: 'production',
  optimization: {
    usedExports: true,
    minimize: true,
  },
  entry: {
    vlcishared: './src/index.js',
  },
  output: {
    path: path.resolve(__dirname, 'dist/js'),
    filename: '[name].js',
    library: '[name]',
    libraryTarget: 'umd',
    clean: true,
  },
  plugins: [
    new webpack.ProvidePlugin({
      Buffer: ['buffer', 'Buffer'],
    }),
    new webpack.ProvidePlugin({
      process: 'process/browser',
    }),
  ],
  externals: {
    echarts: 'echarts',
    'tabulator-tables': 'tabulator-tables',
  },
};
